import matplotlib
import matplotlib.pyplot as plt
import numpy as np

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True

def PPD(PMV):
    return 100 - 95 * np.exp(-(0.03353 * PMV ** 4 + 0.2179 * PMV ** 2))

pmv = np.arange(-3, 3, 0.1)
ppd = PPD(pmv)

plt.figure(figsize=(6,2))
plt.plot(pmv, ppd)
plt.grid(True)
plt.xlabel('PMV')
plt.ylabel(r'PPD [\%]')
#plt.show()
plt.savefig(r'./literature_review/ppd.pgf', dpi=600, bbox_inches='tight', orientation='portrait', papertype='a5')

