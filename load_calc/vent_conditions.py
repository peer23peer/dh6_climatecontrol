import matplotlib as mpl

mpl.rcParams['text.usetex'] = True
mpl.rcParams['text.latex.unicode'] = True

from dimensions import *
from weather import *

from MTIpython.units.SI import u
from MTIpython.material.gas import MoistAir
from MTIpython.mtimath.types import inf
from MTIpython.mtiprint.helper import build_compare_tbl

from dill import load, dump
import numpy as np
import pandas as pd

from copy import deepcopy
import sys
import os
from scipy.optimize import *
from CoolProp.HumidAirProp import HAPropsSI
from CoolProp.CoolProp import PropsSI

u.default_format = '.3~P'
np.set_printoptions(precision=3)

sys.path.extend(os.path.dirname(__file__))

fpath = r'D:\Projects\han\ES_climatecontrol\load_calc\data\results_2014_2017_rev1.sim'
ipath = r'D:\Projects\han\ES_climatecontrol\load_calc\data\inputs.sim'
sppath = r'D:\Projects\han\ES_climatecontrol\load_calc\data\setpoint.sim'
fweather = r'D:\Projects\han\ES_climatecontrol\load_calc\data\history_2010_2017.wet'

load_sp = False

with open(fpath, 'rb') as f:
    data: pd.DataFrame = pd.DataFrame.from_dict(load(f))
data = data.set_index(pd.DatetimeIndex(data['time']))
data = data[data.index.dayofweek < 5]
data = data.between_time('8:00', '22:00')
for k in data.keys():
    if k[-2:] == '.T':
        data[k] = data[k]
    if k[-2:] == '.q' or k[-4:] == '.q_c':
        data[k] = data[k] / 1000
data['outside.T'] = data['roof_0.T']
data['coolload.q'] = -data['coolload.q']
data['season'] = [(date.month % 12 + 3) // 3 for date in data['time']]
data['heat from aula to hall'] = data['source_door_0.q'] + data['source_wallinside_0.q']
data['heat from aula to canteen'] = data['source_door_6.q'] + data['source_wallinside_6.q'] + data[
    'source_wallinside_7.q'] + data['source_wallinside_5.q']
data['heat from aula to ground'] = data['source_floor.q']
data['heat from aula to outside - through roof'] = data['source_roof.q']
data['heat from aula to outside - through walls'] = data['source_outside_wall_0.q'] + data['source_outside_wall_1.q'] + \
                                                    data['source_outside_wall_2.q'] + data['source_outside_wall_3.q'] + \
                                                    data['source_outside_wall_4.q'] + data['source_outside_wall_5.q'] + \
                                                    data['source_window_0.q'] + data['source_window_2.q'] + data[
                                                        'n0_window_sun.q'] + data['n2_window_sun.q']

hb_inside = data[['heat from aula to hall', 'heat from aula to canteen']].resample('M').sum()
hb_inside['sum'] = hb_inside.sum(axis=1)

hb_outside = data[['heat from aula to ground', 'heat from aula to outside - through roof',
                   'heat from aula to outside - through walls']].resample('M').sum()
hb_outside['sum'] = hb_outside.sum(axis=1)

keys = list(data.keys())

# Get weather and simulation data
weather: HistoryWeather = HistoryWeather.load(fweather)

# Get occupation simulation result
with open(ipath, 'rb') as f:
    i_data = dict(load(f))

id = pd.DataFrame.from_dict(i_data)
id = id.set_index(pd.DatetimeIndex(id['time']))
id = id[id.index.dayofweek < 5]
id = id.between_time('8:00', '22:00')

w_data = {
    'time': weather.date[122736:-1],
    'T': weather.T[122736:-1],
    'TD': weather.TD[122736:-1],
    'Q': weather.Q[122736:-1],
    'U': weather.U[122736:-1],
    'P': weather.P[122736:-1]}
wd = pd.DataFrame.from_dict(w_data)
wd = wd.set_index(pd.DatetimeIndex(wd['time']))
wd = wd[wd.index.dayofweek < 5]
wd = wd.between_time('8:00', '22:00')
wd['season'] = [(date.month % 12 + 3) // 3 for date in wd['time']]

days_q95 = wd.loc[wd['T'] > wd['T'].quantile(q=0.95)]['T'].count() / (17 * 14)

q95 = wd.loc[wd['T'] > wd['T'].quantile(q=0.95)]
q95_a = MoistAir(name='Air q95 mean',
                 RH=q95['U'].mean(),
                 T_dp=q95['TD'].mean() * u.degC,
                 T=q95['T'].mean() * u.degC,
                 P=q95['P'].mean() * u.Pa,
                 m=inf * u.kg)

q95 = data.loc[wd['T'] > wd['T'].quantile(q=0.95)]
q95_coolload = 48. * u.kW
q95_heatload = -15. * u.kW
q95_wp = id.loc[wd['T'] > wd['T'].quantile(q=0.95)].loc[id.loc[wd['T'] > wd['T'].quantile(q=0.95)]['person.wp'] > 0][
             'person.wp'].mean() * u.mgr / u.s

air_setpoint = [MoistAir(RH=0.65,
                         T=T * u.degC,
                         P=1. * u.bar) for T in [22., 20., 22., 22., 20.]]

for a_sp in air_setpoint:
    a_sp.m = a_sp.rho * V

season = {0: {'mean': q95_a,
              'coolload': q95_coolload,
              'heatload': q95_heatload,
              'wp': q95_wp}}
for s in [3, 1]:
    mean_a = MoistAir(name='Air mean {}'.format(s),
                      RH=wd.loc[wd['season'] == s]['U'].mean(),
                      T_dp=wd.loc[wd['season'] == s]['TD'].mean() * u.degC,
                      T=wd.loc[wd['season'] == s]['T'].mean() * u.degC,
                      P=wd.loc[wd['season'] == s]['P'].mean() * u.Pa,
                      m=inf * u.kg)
    coolload_max = 48 * u.kW
    heatload_max = -15 * u.kW
    wp = id.loc[data['season'] == s]
    m_wp = wp.loc[wp['person.wp'] > 0]['person.wp'].mean() * u.mgr / u.s
    calc_values = {'mean': mean_a,
                   'coolload': coolload_max,
                   'heatload': heatload_max,
                   'wp': m_wp}
    season[s] = calc_values

ao = [season[s]['mean'] for s in season]
print(build_compare_tbl(ao,['name', 'T', 'T_dp', 'P', 'RH', 'm']))

def mixing(x, q, wp, dt, p_atm, air_aula, T_set, RH_set, alter=True):
    air_a = deepcopy(air_aula)
    T_in = x[0] * u.degC
    m_in = x[1] * u.kg
    RH = x[2]
    Q = q * dt
    m_v_pers = wp * dt

    air_in = MoistAir(name='Air In', P=p_atm, T=T_in, RH=RH, m=m_in)

    # Determine the amount of energy in the air of the aula
    Q_old = air_a.h * air_a.m

    # Remove air that is blown out due to the incomming air and determine the mass of water and dry air that exists in
    # the old air fraction and add the mass of water and dry air that is added to the system
    air_a.m -= air_in.m
    m_v = air_a.m_v + air_in.m_v
    m_a = air_a.m_a + air_in.m_a
    m = m_a + m_v
    # needed because there are floating point errors introduced in the equation
    err_ratio = air_a.m / m
    m_v *= err_ratio
    m_a *= err_ratio
    m_v += m_v_pers
    # Get the new abs humidity ratio kg/kg
    W_new = m_v / m_a
    # Determine the energy that is present in the mixture of old and new humid air
    Q_new = air_a.h * air_a.m + air_in.h * air_in.m
    # Determine the amount of energy that couldn't be migated by adding fresh air
    dQ = Q_old - Q_new
    air_a.m += air_in.m
    # Obtain the new enthalpy of the air, assuming constant pressure
    h_new = Q_new / air_a.m
    # Determine the new temperature due to the enthalpy
    T_new = HAPropsSI('T', 'H', h_new.to('J/kg').m, 'P', air_a.P.to('Pa').m, 'W', W_new.m) * u.K
    # Add the heat up due to residual dQ
    T_new += dQ / (air_a.m * air_a.c_p)
    try:
        # Get the new Relative Humidity
        RH_new = HAPropsSI('RH', 'P', air_a.P.m, 'T', T_new.m, 'W', W_new.m)
    except ValueError:
        # Below the dew point
        T_RH100 = HAPropsSI('T', 'P', air_a.P.m, 'RH', 1., 'H', h_new.m) * u.K
        W_fog = HAPropsSI('W', 'P', air_a.P.m, 'RH', 1., 'H', h_new.m) - air_a.W
        RH_new = 1.
    air_a.T = T_new
    air_a.RH = RH_new

    # Create the residual state vector, needed to optimize
    #q_residual = ((Q - dQ) / Q).m
    T_residual = 10 * ((T_set - air_a.T) / air_a.T).m
    RH_residual = (RH_set - air_a.RH) / RH_set

    # print(air_a)
    # print('T_res: {}\nRH_res: {}'.format(T_residual, RH_residual))
    if not alter:
        return np.linalg.norm(np.array([T_residual, RH_residual]))
    else:
        return air_a


if load_sp:
    with open(sppath, 'rb') as f:
        sp = dict(load(f))
else:
    sp = {}
    for s in [0, 3, 1]:
        dt = 5 * u.min
        T_min = (air_setpoint[s].T - 10. * u.delta_degC).to(u.degC).m
        T_max = air_setpoint[s].T.to(u.degC).m
        m_in_min = 1 * (air_setpoint[s].m / u.hr * dt).to(u.kg).m
        m_in_max = 10 * (air_setpoint[s].m / u.hr * dt).to(u.kg).m
        RH_min = air_setpoint[s].RH - air_setpoint[s].RH * 0.1
        RH_max = air_setpoint[s].RH + air_setpoint[s].RH * 0.1

        x = fmin_tnc(mixing, x0=np.array([T_max, m_in_min, RH_min]), args=(
            season[s]['coolload'], season[s]['wp'], dt, season[s]['mean'].P, air_setpoint[s], season[s]['mean'].T,
            season[s]['mean'].RH, False), bounds=np.array([[T_min, T_max], [m_in_min, m_in_max], [RH_min, RH_max]]),
                     approx_grad=True, epsilon=0.001, ftol=1e-11)

        sp[s] = {
            'air_in': MoistAir(name='Air In', P=season[3]['mean'].P, T=x[0][0] * u.degC, RH=x[0][2], m=x[0][1] * u.kg),
            'air_aula': mixing(x[0], season[s]['coolload'], season[s]['wp'], dt, season[s]['mean'].P, air_setpoint[s],
                               season[s]['mean'].T,
                               season[s]['mean'].RH, True)}

        print(x)

    with open(sppath, 'wb') as f:
        dump(sp, f)

ai = [sp[s]['air_in'] for s in sp]
for m in ai:
    m.vs = (m.V / u.hr).to('m**3/s')
print(build_compare_tbl(ai, ['T', 'T_dp', 'P', 'RH', 'vs']))

aula = [sp[s]['air_aula'] for s in sp]
print(build_compare_tbl(aula, ['T', 'T_dp', 'P', 'RH']))


sp_h = {}
for s in [0, 3, 1]:
    dt = 5 * u.min
    T_min = (air_setpoint[s].T - 10. * u.delta_degC).to(u.degC).m
    T_max = air_setpoint[s].T.to(u.degC).m
    m_in_min = 1 * (air_setpoint[s].m / u.hr * dt).to(u.kg).m
    m_in_max = 10 * (air_setpoint[s].m / u.hr * dt).to(u.kg).m
    RH_min = air_setpoint[s].RH - air_setpoint[s].RH * 0.1
    RH_max = air_setpoint[s].RH + air_setpoint[s].RH * 0.1

    x = fmin_tnc(mixing, x0=np.array([T_max, m_in_min, RH_min]), args=(
        season[s]['heatload'], season[s]['wp'], dt, season[s]['mean'].P, air_setpoint[s], season[s]['mean'].T,
        season[s]['mean'].RH, False), bounds=np.array([[T_min, T_max], [m_in_min, m_in_max], [RH_min, RH_max]]),
                 approx_grad=True, epsilon=0.001, ftol=1e-11)

    sp_h[s] = {
        'air_in': MoistAir(name='Air In', P=season[3]['mean'].P, T=x[0][0] * u.degC, RH=x[0][2], m=x[0][1] * u.kg),
        'air_aula': mixing(x[0], season[s]['heatload'], season[s]['wp'], dt, season[s]['mean'].P, air_setpoint[s],
                           season[s]['mean'].T,
                           season[s]['mean'].RH, True)}

    print(x)

ai = [sp_h[s]['air_in'] for s in sp_h]
for m in ai:
    m.vs = (m.V / u.hr).to('m**3/s')
print(build_compare_tbl(ai, ['T', 'T_dp', 'P', 'RH', 'vs']))

aula = [sp_h[s]['air_aula'] for s in sp_h]
print(build_compare_tbl(aula, ['T', 'T_dp', 'P', 'RH']))