from copy import deepcopy
from scipy.optimize import minimize

from MTIpython import u
from MTIpython.material import material_factory

# %% Known variables
coolingloads = [50., -15.] * u.kW
RH = [0.5, 0.4]
air_entree = material_factory['air_humid']
persons = 100. * 4
airflow_per_person = 43.3 * u.m ** 3 / u.hr
airflow_total = airflow_per_person * persons

# Conditions aula
temperatures = [20, 23] * u.degC
for temperature, rh, coolingload in zip(temperatures, RH, coolingloads):
    air_aula = material_factory['air_humid']
    air_aula.V = 403.76 * u.m ** 2 * 6. * u.m
    air_aula.T = temperature
    air_aula.RH = 0.5

    # Function to optimize
    def f(params):
        # Set inlet air conditions
        air_e = deepcopy(air_entree)
        air_e.T = params[0] * u.degC
        air_e.RH = params[1]
        air_e.V = airflow_total * u.s
        # Update aula air conditions
        air_a = deepcopy(air_aula)
        air_a.m = air_e.m
        res = (air_a.h - air_e.h).to('J/kg').m * air_e.m.to('kg').m
        print("load: {}".format(res))
        return abs(coolingload.to('W').m - res)

    # solve
    x0 = [19.,rh]
    bnds = ((15., 29.),(rh,rh))
    res = minimize(f, x0, method='SLSQP', bounds=bnds)
    print("x: f({}) -> {}".format(res.x, f(res.x)))
