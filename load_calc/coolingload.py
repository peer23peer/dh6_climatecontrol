from weather import *
from dimensions import *
from materials import *
from components import *
from simulation import *

from MTIpython.units.SI import u
from MTIpython.thermo.core import *
from MTIpython.thermo.network import *
from MTIpython.thermo.NEN5067 import *
from MTIpython.core import logging as mtilogging

mtilogging.setup_logging()
import logging

logging.getLogger().setLevel(logging.INFO)
mtilogging.getLogger('MTIpython.thermo.network').setLevel(logging.DEBUG)


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from dill import dump, load
from datetime import datetime

u.default_format = '~P'
np.set_printoptions(precision=3)

fpath = r'D:\Projects\han\ES_climatecontrol\load_calc\data\results_2014_2017_rev1.sim'
opath = r'D:\Projects\han\ES_climatecontrol\load_calc\data\occupation_rev1.sim'
fweather = r'D:\Projects\han\ES_climatecontrol\load_calc\data\history_2010_2017.wet'
inpath = r'D:\Projects\han\ES_climatecontrol\load_calc\data\inputs.sim'

simulate = True

# === Build the model ===========

aula = HeatStoreNode(name='aula', material=air_aula)
cavity_north = HeatFlowNode(name='cavity_north')
cavity_south = HeatFlowNode(name='cavity_south')

coolload = HeatSourceNode(name='coolload')

# === Internal =================
female = {'clothing': 0.65 * u.clo,
          'metabolisme': Metabolisme.SITTING_WORK,
          'number of': 25}
male = {'clothing': 0.8 * u.clo,
        'metabolisme': Metabolisme.SITTING_WORK,
        'number of': 75}
persons_q = female['number of'] * P_p(clo=female['clothing'], T_v=22. * u.degC, M=female['metabolisme']) + \
            male['number of'] * P_p(clo=male['clothing'], T_v=22. * u.degC, M=male['metabolisme'])

persons_wp = female['number of'] * w_p(clo=female['clothing'], T_v=22. * u.degC, M=female['metabolisme']) + \
             male['number of'] * w_p(clo=male['clothing'], T_v=22. * u.degC, M=male['metabolisme'])

persons = HeatWaterSourceNode(name='persons', q=persons_q, w_p=persons_wp)
lamps = HeatSourceNode(name='lamps', q=1000. * u.W)

# === Connect the components ==========
model = Model(nodes=[aula, cavity_north, cavity_south, persons, lamps, coolload])
Model.add_edge(model.G, 'persons', 'aula', Edge())
Model.add_edge(model.G, 'lamps', 'aula', Edge())
Model.add_edge(model.G, 'coolload', 'aula', Edge())

# Roof and floor
build_roof(A=surface, G=model.G, sink='aula')
build_floor(A=surface, G=model.G, sink='aula')

# Wall outside 0
build_outside_wall(name='0', G=model.G, A=outside[0]['stone'], sink='aula')
build_outside_window(name='0', G=model.G, A=outside[0]['window'], sink='aula')

# Wall outside 1
build_outside_wall(name='1', G=model.G, A=outside[1]['stone'], sink='cavity_north')

# Wall outside 2
build_outside_wall(name='2', G=model.G, A=outside[2]['stone'], sink='aula')
build_outside_window(name='2', G=model.G, A=outside[2]['window'], sink='aula')

# Wall outside 3
build_outside_wall(name='3', G=model.G, A=outside[3]['stone'], sink='cavity_south')

# Wall outside 4
build_outside_wall(name='4', G=model.G, A=outside[4]['stone'], sink='cavity_north')

# Wall outside 4
build_outside_wall(name='5', G=model.G, A=outside[5]['stone'], sink='cavity_south')

# Wall inside 0
build_inside_wall(name='0', G=model.G, A=inside[0]['stone'], sink='aula')
build_door(name='0', G=model.G, A=inside[0]['door'], sink='aula')

# Wall inside 1
build_inside_wall(name='1', G=model.G, A=inside[1]['stone'], sink='aula', source='cavity_south')

# Wall inside 2
build_inside_wall(name='2', G=model.G, A=inside[2]['stone'], sink='aula', source='cavity_north')

# Wall inside 3
build_inside_wall(name='3', G=model.G, A=inside[3]['stone'], sink='aula', source='cavity_south')

# Wall inside 4
build_inside_wall(name='4', G=model.G, A=inside[4]['stone'], sink='aula', source='cavity_north')

# Wall inside 5
build_inside_wall(name='5', G=model.G, A=inside[5]['stone'], sink='aula')

# Wall inside 6
build_inside_wall(name='6', G=model.G, A=inside[6]['stone'], sink='aula')
build_door(name='6', G=model.G, A=inside[6]['door'], sink='aula')

# Wall inside 7
build_inside_wall(name='7', G=model.G, A=inside[7]['stone'], sink='aula')

model.init()

# == Set constrains ==============
T_outside = 25. * u.degC
T_ground = 18. * u.degC
T_aula = 22. * u.degC
T_hall = 20. * u.degC
T_canteen = 22. * u.degC

model.aula.T = T_aula
model.aula.q_c = 0. * u.W
model.aula.material.m = V * model.aula.material.rho

model.floor_0.T = T_ground
model.roof_0.T = T_outside

model.n0_walloutside_0.T = T_outside
model.n0_window_0.T = T_outside
model.n0_window_sun.q = 1000. * u.W

model.n1_walloutside_0.T = T_outside

model.n2_walloutside_0.T = T_outside
model.n2_window_0.T = T_outside
model.n2_window_sun.q = 400. * u.W

model.n3_walloutside_0.T = T_outside

model.n4_walloutside_0.T = T_outside

model.n5_walloutside_0.T = T_outside

model.n0_wallinside_2.T = T_hall
model.n0_door_2.T = T_hall

model.n5_wallinside_2.T = T_canteen

model.n6_wallinside_2.T = T_canteen
model.n6_door_2.T = T_canteen

model.n7_wallinside_2.T = T_canteen

res = model.solve()

# === Set-up the simulation =====
s_time = 122736 #* 24 * 31
e_time = -1 #s_time + 1 * 24 * 1 * 31

if simulate:
    weather: HistoryWeather = HistoryWeather.load(fweather)
    dates = weather.date[s_time:e_time]
    T_outside = weather.T[s_time:e_time]
    Q = weather.Q[s_time:e_time]
    lights = simulate_light(dates, surface)
    T_set = simulate_aula_T_set(dates)
    T_canteen = simulate_temperature_canteen(dates)
    T_hall = simulate_temperature_hall(dates)
    T_ground = simulate_temperature_ground(dates, T_outside)
    n0_window_sun = simulate_sun(dates, Q, Orientation.W, A=outside[0]['window'], A_w=A_w)
    n2_window_sun = simulate_sun(dates, Q, Orientation.N, A=outside[2]['window'], A_w=A_w)
    occupation = simulate_occupation(dates, 75, 30, T_outside, T_set, Metabolisme.SITTING_WORK)

    input_dict = {
        'time': dates,
        'T_outside': T_outside,
        'Q': Q,
        'lights': lights,
        'T_set': T_set,
        'T_canteen': T_canteen,
        'T_hall': T_hall,
        'T_ground': T_ground,
        'n0_window_sun': 2 * n0_window_sun,
        'n2_window_sun': 2 * n2_window_sun,
        'person.q': occupation[0],
        'person.wp': occupation[1]
    }

    with open(inpath, 'wb') as f:
        dump(input_dict, f)

else:
    with open(inpath, 'rb') as f:
        siminputs = load(f)

    dates = siminputs['time'][s_time:e_time]
    T_outside = siminputs['T_outside'][s_time:e_time]
    Q = siminputs['Q'][s_time:e_time]
    lights = siminputs['lights'][s_time:e_time]
    T_set = siminputs['T_set'][s_time:e_time]
    T_canteen = siminputs['T_canteen'][s_time:e_time]
    T_hall = siminputs['T_hall'][s_time:e_time]
    T_ground = siminputs['T_ground'][s_time:e_time]
    n0_window_sun = siminputs['n0_window_sun'][s_time:e_time]
    n2_window_sun = siminputs['n2_window_sun'][s_time:e_time]
    occupation = [siminputs['person.q'][s_time:e_time], siminputs['person.wp'][s_time:e_time]]

# coolload_q = simulate_coolload_set(dates)

start_time = datetime.now()
results = model.simulate(
    time=dates,
    time_step=1. * u.hr,
    aula=('T', T_set),
    # coolload=('q', coolload_q),
    n0_walloutside_0=('T', T_outside),
    n0_window_sun=('q', n0_window_sun),
    n1_walloutside_0=('T', T_outside),
    n2_walloutside_0=('T', T_outside),
    n2_window_sun=('q', n2_window_sun),
    n3_walloutside_0=('T', T_outside),
    n4_walloutside_0=('T', T_outside),
    n5_walloutside_0=('T', T_outside),
    roof_0=('T', T_outside),
    floor_0=('T', T_ground),
    lamps=('q', lights),
    persons=('q', occupation[0]),
    n0_wallinside_2=('T', T_hall),
    n0_door_2=('T', T_hall),
    n5_wallinside_2=('T', T_canteen),
    n6_wallinside_2=('T', T_canteen),
    n6_door_2=('T', T_canteen),
    n7_wallinside_2=('T', T_canteen)
)
end_time = datetime.now()

results['sim_start_time'] = start_time
results['sim_end_time'] = end_time
results['sim_run_time'] = end_time - start_time

with open(fpath, 'wb') as f:
    dump(results, f)

fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
year = mdates.YearLocator()
month = mdates.MonthLocator()
fmt = mdates.DateFormatter('%y-%m-%d')
plt.plot(results['time'], results['n0_walloutside_0.T'].to(u.degC), 'r')
plt.plot(results['time'], results['aula.T'].to(u.degC), 'g')
ax2.plot(results['time'], results['coolload.q'].to(u.kW), 'b')
ax1.xaxis.set_major_locator(year)
ax1.xaxis.set_major_formatter(fmt)
ax1.xaxis.set_minor_locator(month)
plt.grid(True, axis='both')
plt.show()
x = 0
