% !TeX root = MTIreport.tex
% !TeX spellcheck = en-US
\chapter{Thermal comfort}\label{chap:Thermal comfort}
Thermal comfort has been defined by Hensen as "a state in which there are no driving impulses to correct the environment by the behavior" \cite{hensen_thermal_1991}. The American Society of Heating, Refrigerating and Air-Conditioning Engineers (ASHRAE) defined it as "the condition of the mind in which satisfaction is expressed with the thermal environment" \cite{ansi/ashrae_standard_2017}. As such, it will be influenced by personal differences in mood, culture and other individual, organizational and social factors. Based on the above definitions, comfort is not a state condition, but rather a state of mind \cite{djongyang_thermal_2010}.

At present, two different approaches for the definition of thermal comfort coexist, each one with its potentialities and limits: the rational or heat-balance approach and the adaptive approach \cite{doherty_evaluation_1988}. The rational approach uses data from climate chamber studies to support its theory, best characterized by the works of Fanger while the adaptive approach uses data from field studies of people in building \cite{kwok_addressing_2010}.

Steady-state experiments showed that, cold discomfort is strongly related to the mean skin temperature and that warmth discomfort is strongly related to the skin wettedness caused by sweat secretion. Dissatisfaction may be caused by the body as a whole being too warm or cold, or by unwanted heating or cooling of a particular part of the body (local discomfort) \cite{hensen_thermal_1991}. These relations are the basis for methods like, Fanger's \cite{fanger_thermal_1970-1} comfort model that incorporates the six factors mentioned by Macpherson, and the two-node model of \citeauthor{gagge_standard_1986} \cite{gagge_standard_1986}.

The heat-balance approach is based on Fanger's experiments \cite{fanger_thermal_1970-1} in controlled climate chamber on 1296 young Danish students, using a steady-state heat transfer model. In these studies, participants were dressed in standardized clothing and completed standardized activities, while exposed to different thermal environments. In some studies the thermal conditions were chosen, and participants recorded how hot or cold they felt, using the seven-point \acrfull{ASHRAE} thermal sensation scale ranging from cold $ −3 $ to hot $ +3 $ with neutral $ 0 $ in the middle. In other studies, participants controlled the thermal environment themselves, adjusting the temperature until they felt thermally "neutral" \cite{charles_fangers_2003}.

That comfort equation was expanded \cite{fanger_thermal_1970-1} using data from 1296 participants. The resulting equation described thermal comfort as the imbalance between the actual heat flow from the body in a given thermal environment and the heat flow required for optimum (i.e. neutral) comfort for a given activity. This expanded equation related thermal conditions to the seven-point \acrshort{ASHRAE} thermal sensation scale, and became known as the \acrfull{PMV} index. The \acrshort{PMV} was then incorporated into the \acrfull{PPD} index. Fanger's \acrshort{PMV}-\acrshort{PPD} model on thermal comfort has been a path breaking contribution to the theory of thermal comfort and to the evaluation of indoor thermal environments in buildings. It is widely used and accepted for design and field assessment of thermal comfort \cite{lin_study_2008}.

\section{\acrfull{PMV}}

Fanger related \acrshort{PMV} to the imbalance between the actual heat flow from a human body in a given environment and the heat flow required for optimum comfort at a specified activity by equation \ref{eq:pmv fanger} \cite{lin_study_2008}. Where \gls{s:L} is the \glsdesc{s:L}, defined as the difference between internal heat production and heat loss to the environment for a person and \gls{s:M} is \glsdesc{s:M}. Which can be simplified to \gls{s:L} multiplied with \gls{s:alpha_s} which is a \glsdesc{s:alpha_s} \cite{djongyang_thermal_2010}.

\begin{equation}\label{eq:pmv fanger}
    \acrshort{PMV} = \left[0.303 e^{\left(-0.036\gls{s:M}\right)} + 0.028\right]\gls{s:L} = \gls{s:alpha_s} \gls{s:L}
\end{equation}

The Institute for Environmental Research of the State University of Kansas, under \acrshort{ASHRAE} contract, has conducted extensive research on the subject of thermal comfort in sedentary regime. The purpose of this investigation was to obtain a model to express the \acrshort{PMV} in terms of parameters easily sampled in an environment. The results have yielded to equation \ref{eq:pmv useful} \cite{orosa_research_2009}.

\begin{equation}\label{eq:pmv useful}
    \acrshort{PMV} = \gls{s:a} \gls{s:T} + \gls{s:b} \gls{s:p_v} - \gls{s:c}
\end{equation}

Where \gls{s:p_v} is \glsdesc{s:p_v} and \gls{s:T} is \glsdesc{s:T}. Coefficients \gls{s:a}, \gls{s:b}, \gls{s:c} are given in table \ref{tbl:coefficientsabc}. With these criteria it has been given a comfort zone that, on average, is close to conditions of $ 26 \si{\degreeCelsius} $ and $ 50 \si{\percent} $ relative humidity. This study was undergone with subjects to a sedentary metabolic activity, dressed with normal clothes and with a thermal resistance of approximately $ 0.6 \si{\clothing} $, and with exposure to the indoor ambiance of $ 3 \si{\hour} $.

\begin{table}[!htbp]
    \centering
    \begin{tabular}{l l l l} \hline
        Time/sex & \gls{s:a} & \gls{s:b} & \gls{s:c} \\ \hline
        1 h/man  & 0.220     & 0.233     & 6.673     \\
        Woman    & 0.272     & 0.248     & 7.245     \\
        Both     & 0.245     & 0.248     & 6.475     \\ \hline
        2 h/man  & 0.221     & 0.270     & 6.024     \\
        Woman    & 0.283     & 0.210     & 7.694     \\
        Both     & 0.252     & 0.240     & 6.859     \\ \hline
        3 h/man  & 0.212     & 0.293     & 5.949     \\
        Woman    & 0.275     & 0.255     & 8.620     \\
        Both     & 0.243     & 0.278     & 8.802     \\ \hline
    \end{tabular}
    \caption{Values of the coefficients \gls{s:a}, \gls{s:b} and \gls{s:c} as a function of spent time and the sex of the subject \cite{orosa_research_2009}}\label{tbl:coefficientsabc}
\end{table}

\section{\acrfull{PPD}}
The \acrshort{PPD} predicts the percentage of the people who felt more than slightly warm or slightly cold (i.e. the percentage of the people who inclined to complain about the environment). Using the seven-point scale of thermal sensation $ −3 ... +3 $, Fanger \cite{fanger_thermal_1970} postulated: are declared uncomfortable all those who responded $ \pm 2 $ and $ \pm 3 $. Those who responded $ \pm 1 $ and $ 0 $ are declared comfortable. The percentages of subjects who responded $ \pm 2 $ and $\pm 3 $ are determined for each class of \acrshort{PMV}; that variable has been called \acrshort{PPD}. The relationship between \acrshort{PPD} and \acrshort{PMV} is given by \cite{djongyang_thermal_2010} equation \ref{eq:PPD}.

\begin{equation}\label{eq:PPD}
    \acrshort{PPD} = 100 - 95 e^{-\left(0.03353 \acrshort{PMV}^4 + 0.2179 \acrshort{PMV}^2\right)}
\end{equation}

The merit of this relation is that, it reveals a perfect symmetry with respect to thermal neutrality $ \acrshort{PMV} = 0 $  It can be seen in figure \ref{fig:pmv vs ppd} that, even when the \acrshort{PMV} index is  $ 0 $, there are some individual cases of dissatisfaction with the level of temperature, although all are dressed in a similar way and that the level of activity is the same.

\begin{figure}
    \centering
    \input{ppd.pgf}
    \caption{Relationship \acrshort{PMV} versus \acrshort{PPD}}\label{fig:pmv vs ppd}
\end{figure}

Thermal comfort standards use the \acrshort{PMV} model to recommend acceptable thermal comfort conditions. The recommendations made by \acrshort{ASHRAE} Standard 55 are shown in table \ref{tbl:ashrae standard recommendations}. These conditions were assumed for a relative humidity of $ 50 \si{\percent} $, a mean relative velocity lower than $ 0.15 \si{\meter\per\second} $, a mean radiant temperature equal to air temperature and a metabolic rate of $ 1.2 \si{\metabolicrate} $. Clothing insulation was defined as $ 0.9 \si{\clothing} $ in winter and $ 0.5 \si{\clothing} $ in summer \cite{orosa_research_2009}.

\begin{table}{!htbp}
    \centering
    \begin{tabular}{l l l} \hline
               & Operative temperature        & Acceptable range                  \\ \hline
        Summer & $ 22 \si{\degreeCelsius} $   & $ 20 ... 23 \si{\degreeCelsius} $ \\
        Winter & $ 24.5 \si{\degreeCelsius} $ & $ 23 ... 26 \si{\degreeCelsius} $ \\ \hline
    \end{tabular}
    \caption{\acrshort{ASHRAE} Standard recommendations \cite{orosa_research_2009}}\label{tbl:ashrae standard recommendations}
\end{table}

\section{Conclusion}
With people involved in near-sedentary activity and steady-state conditions, rational approach easily and reasonably produces accurate predictions of occupant thermal sensation, but it is not always a good predictor of actual thermal sensation, particularly in field study settings \cite{kumar_thermal_2010}.  One should be very careful when interpreting the results of thermal comfort campaigns. The actual standards help but should not be considered as absolute references. In fact, the individual state of mind that expresses satisfaction with the thermal environment is too diverse for that when small groups are considered \cite{djongyang_thermal_2010}.
