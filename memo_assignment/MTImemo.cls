\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{MTImemo}[2017/12/16 v1.1 MTI memo class]

\newif\if@dutch
\newif\if@english
\DeclareOption{dutch}{\@dutchtrue\@englishfalse}
\DeclareOption{english}{\@dutchfalse\@englishtrue}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ExecuteOptions{english}
\ProcessOptions\relax

\LoadClass[10pt,a4paper]{article}

\if@dutch
\RequirePackage[dutch]{babel}
\else
\RequirePackage[dutch,english]{babel}
\fi

\RequirePackage{graphicx}
\RequirePackage[left=2.25cm,right=2.5cm,top=1cm,bottom=6.2cm]{geometry}

\usepackage{lipsum}
\usepackage{lastpage}
\usepackage{afterpage}
\usepackage{titlesec}
\usepackage{cancel}
\usepackage{xparse}
\usepackage{blindtext}
\usepackage{amsmath}
\usepackage{bm}
\usepackage[hidelinks,hyperfootnotes=false]{hyperref}
\usepackage{tabu}
\usepackage[svgnames]{xcolor}
\usepackage{float}
\usepackage{multirow}
\usepackage{calc}
\usepackage{booktabs}
\usepackage{eso-pic}
\usepackage{incgraph}
\usepackage{url}
\usepackage{csquotes}
\usepackage{etoolbox}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{tabularx}
\usepackage{chemformula}
\usepackage{physics}

%==========font================================================================
\usepackage[default]{lato}
\usepackage[T1]{fontenc}
\definecolor{mti_gray}{HTML}{A9A9A9}

% === Heading styles =========================================================
\titleformat{\chapter}{\normalfont\huge}{\thechapter.}{20pt}{\huge\it}
\titlespacing*{\chapter}{0cm}{0cm}{1cm}

%==========Bibliography========================================================
\usepackage[
backend=bibtex,
style=ieee,
sorting=ynt
]{biblatex}

\RequirePackage{fancyhdr}
\setlength{\headheight}{3.5cm}
\fancyhead[R,RO]{\textbf{IHC MTI B.V.}}
\fancyhead[L,LO]{\includegraphics[height=2cm]{resources/MTIlogo.png}}
\fancyfoot[C,CO]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\pagestyle{fancy}

\newcommand{\memoto}[1]{\def\@memoto{#1}}
\newcommand{\memofrom}[1]{\def\@memofrom{#1}}
\newcommand{\memodate}[1]{\def\@memodate{#1}}
\newcommand{\memoref}[1]{\def\@memoref{#1}}
\newcommand{\memosubject}[1]{\def\@memosubject{#1}}

\newcommand{\makeheader}{
	\if@dutch
		\noindent \textbf{{\large Memo}} \\[1cm]
		\noindent Aan \\
		\@memoto \\[0.5cm]
		\noindent Van \\
		\@memofrom \\[1.5cm]
		\noindent
		\begin{tabular}{ll}
			Datum & Referentie \\
			\@memodate & \@memoref
		\end{tabular} \\[0.75cm]
		Onderwerp \\
		\textbf{\@memosubject} \\[1.5cm]
	\else
		\noindent \textbf{{\large Memo}} \\[1cm]
		\noindent To \\
		\@memoto \\[0.5cm]
		\noindent From \\
		\@memofrom \\[1.5cm]
		\noindent
		\begin{tabular}{ll}
			Date & Reference \\
			\@memodate & \@memoref
		\end{tabular} \\[0.75cm]
		\noindent Subject \\
		\noindent \textbf{\@memosubject} \\[1.5cm]
	\fi
	}

% ========== Units =============================================================
\usepackage[per-mode=fraction,
			separate-uncertainty=true,
			bracket-unit-denominator=false,
			multi-part-units=single]{siunitx}
\newcommand{\glsmath}[1]{\ensuremath{\gls{#1}\mathbin{\color{mti_gray}\left[\glsunit{#1}\right]}}}

\DeclareSIUnit{\clothing}{clo}
\DeclareSIUnit{\metabolicrate}{met}
\DeclareSIUnit{\person}{pers}

%============Glossary===========================================================
\usepackage[acronym,toc]{glossaries}

\glsaddkey{unit}{\glsentrytext{\glslabel}}{\glsentryunit}{\GLsentryunit}{\glsunit}{\Glsunit}{\GLSunit}
\newglossary[slg]{symbolslist}{sys}{syo}{Symbolslist}

\setlength{\glsdescwidth}{11cm}
\setlength{\LTleft}{0pt}
%\glspagelistwidth{13cm}
\newglossarystyle{symbunitlong}{%
	\setglossarystyle{long3col}% base this style on the list style
	\renewenvironment{theglossary}{% Change the table type --> 3 columns
		\begin{longtable}{lp{\glsdescwidth}>{\centering\arraybackslash}p{2cm}}}%
		{\end{longtable}}
	\renewcommand*{\glossaryheader}{%  Change the table header
		\bfseries Sign & \bfseries Description & \bfseries Unit \\
		\hline
		\endhead}
	\renewcommand*{\glossentry}[2]{%  Change the displayed items
		\glstarget{##1}{\glossentryname{##1}} & \glossentrydesc{##1} & \glsunit{##1}  \tabularnewline
	}
}

\newglossarystyle{mtilong}{
	\setglossarystyle{long3col}
	\renewenvironment{theglossary}{
		\begin{longtable}{lp{\glsdescwidth}>{\arraybackslash}p{2cm}}}
		{\end{longtable}}
	\renewcommand*{\glossaryheader}{
		\bfseries Key & \bfseries Description & \bfseries Page \\
		\hline
		\endhead}
	\renewcommand*{\glossentry}[2]{
		\glstarget{##1}{\glossentryname{##1}} & \glossentrydesc{##1} & {##2} \tabularnewline
	}
}

\makeglossaries

\newcommand{\printmtiglossary}{
	\clearpage
	\printglossary[type=symbolslist,style=symbunitlong]
	\vspace{20mm} %30mm vertical space
	\printglossary[type=\acronymtype,style=mtilong]
	\vspace{20mm} %30mm vertical space
	\printglossary[type=main,style=mtilong]
	\vspace{20mm} %30mm vertical space
}

% ===TIKZ=======================================================================
\usepackage{tikz}
\usepackage{pgf}
\usepackage[siunitx,rotatelabels]{circuitikz}
\usetikzlibrary{shapes,arrows,positioning,calc,circuits}
\usetikzlibrary{decorations.markings}
\usetikzlibrary{shapes.geometric}

\tikzset{
	block/.style = {draw, fill=white, rectangle, minimum height=3em, minimum width=3em, rounded corners=3mm, fill=gray!20},
	tmp/.style  = {coordinate},
	sum/.style= {draw, fill=white, circle, node distance=1cm},
	input/.style = {draw, coordinate},
	output/.style= {coordinate},
	pinstyle/.style = {pin edge={to-,thin,black}
	}
}

\pgfdeclarelayer{edgelayer}
\pgfdeclarelayer{nodelayer}
\pgfsetlayers{edgelayer,nodelayer,main}

\tikzstyle{none}=[inner sep=0pt, align=center]
\tikzstyle{dot}=[circle,draw,inner sep=1pt,fill=black]

\tikzstyle{ct_ground}=[ground]
\tikzstyle{ct_vcc}=[vcc]
\tikzstyle{ct_vee}=[vee]
\tikzstyle{ct_spdt}=[spdt]
\tikzstyle{ct_left}=[left]
\tikzstyle{ct_right}=[right]
\tikzstyle{block}=[draw, fill=white, rectangle, minimum height=3em, minimum width=3em, rounded corners=3mm, fill=gray!20, align=center]

\tikzstyle{simple}=[-,draw=Black]
\tikzstyle{ct_R}=[R]
\tikzstyle{ct_L}=[L]
\tikzstyle{ct_open}=[open]
\tikzstyle{ct_short}=[short]
\tikzstyle{arrow}=[-,draw=Black,postaction={decorate},decoration={markings,mark=at position .5 with {\arrow{>}}},line width=2.000]


% ===ENVIRONMENTS===============================================================
\usepackage[most]{tcolorbox}

\newtcolorbox[auto counter]{statement}[1][]{%
  enhanced jigsaw, % better frame drawing
  borderline west={2pt}{0pt}{red}, % straight vertical line at the left edge
  sharp corners, % No rounded corners
  boxrule=0pt, % no real frame,
  fonttitle={\large\bfseries},
  coltitle={black},  % Black color for title
  title={Statement of needs:\\ },  % Fixed title
  attach title to upper, % Move the title into the box
  #1
}
