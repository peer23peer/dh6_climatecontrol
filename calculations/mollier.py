from psychrochart.chart import PsychroChart

# Load default style:
chart = PsychroChart('ashrae')
chart.figure_params['title'] = 'Psychrometric Chart Q95 Summer conditions'
chart.figure_params['figsize'] = (11, 8)
zones_conf = {
    "zones": [{
        "zone_type": "dbt-rh",
        "style":     {"edgecolor": [1.0, 0.749, 0.0, 0.8],
                      "facecolor": [1.0, 0.749, 0.0, 0.2],
                      "linewidth": 2,
                      "linestyle": "--"},
        "points_x":  [18, 22],
        "points_y":  [30, 60],
        "label":     ""
    },
    ]}
chart.append_zones(zones_conf)

points = {'outside':     {'label': 'Estimated (Weather service)',
                          'style': {'color':  [0.855, 0.004, 0.278, 0.8],
                                    'marker': 'X', 'markersize': 15},
                          'xy':    (26.3, 49.3)},
          'inlet_100':       {
              'label': 'Inlet wetbulb temperature',
              'style': {'color':  [0.573, 0.106, 0.318, 0.5],
                        'marker': 'x', 'markersize': 15},
              'xy':    (10.05, 100.0)},
          'inlet': {
              'label': 'Inlet airstream',
              'style': {'color':  [0.573, 0.106, 0.318, 0.5],
                        'marker': 'x', 'markersize': 15},
              'xy':    (15.48, 50.0)},
          'interior':    {'label': 'Interior',
                          'style': {'color':  [0.592, 0.745, 0.051, 0.9],
                                    'marker': 'o', 'markersize': 15},
                          'xy':    (20, 50)}}
connectors = [{'start': 'outside',
               'end':   'inlet_100',
               'style': {'color':     [0.573, 0.106, 0.318, 0.7],
                         "linewidth": 2, "linestyle": "-."}},
              {'start': 'inlet_100',
               'end':   'inlet',
               'style': {'color':     [0.573, 0.106, 0.318, 0.7],
                         "linewidth": 2, "linestyle": "-."}},
              {'start': 'inlet',
               'end':   'interior',
               'style': {'color':     [0.855, 0.145, 0.114, 0.8],
                         "linewidth": 2, "linestyle": ":"}}]
chart.plot_points_dbt_rh(points, connectors)

chart.plot_legend(markerscale=.7, frameon=False, fontsize=10, labelspacing=1.2);
chart.save('mollier_q95.png')
chart.close_fig()

chart = PsychroChart('ashrae')
chart.figure_params['title'] = 'Psychrometric Chart Summer conditions'
chart.figure_params['figsize'] = (11, 8)
zones_conf = {
    "zones": [{
        "zone_type": "dbt-rh",
        "style":     {"edgecolor": [1.0, 0.749, 0.0, 0.8],
                      "facecolor": [1.0, 0.749, 0.0, 0.2],
                      "linewidth": 2,
                      "linestyle": "--"},
        "points_x":  [18, 22],
        "points_y":  [30, 60],
        "label":     ""
    },
    ]}
chart.append_zones(zones_conf)

points = {'outside':     {'label': 'Estimated (Weather service)',
                          'style': {'color':  [0.855, 0.004, 0.278, 0.8],
                                    'marker': 'X', 'markersize': 15},
                          'xy':    (19., 69.6)},
          'inlet_100':       {
              'label': 'Inlet wetbulb temperature',
              'style': {'color':  [0.573, 0.106, 0.318, 0.5],
                        'marker': 'x', 'markersize': 15},
              'xy':    (10.05, 100.0)},
          'inlet': {
              'label': 'Inlet airstream',
              'style': {'color':  [0.573, 0.106, 0.318, 0.5],
                        'marker': 'x', 'markersize': 15},
              'xy':    (15.48, 50.0)},
          'interior':    {'label': 'Interior',
                          'style': {'color':  [0.592, 0.745, 0.051, 0.9],
                                    'marker': 'o', 'markersize': 15},
                          'xy':    (20, 50)}}
connectors = [{'start': 'outside',
               'end':   'inlet_100',
               'style': {'color':     [0.573, 0.106, 0.318, 0.7],
                         "linewidth": 2, "linestyle": "-."}},
              {'start': 'inlet_100',
               'end':   'inlet',
               'style': {'color':     [0.573, 0.106, 0.318, 0.7],
                         "linewidth": 2, "linestyle": "-."}},
              {'start': 'inlet',
               'end':   'interior',
               'style': {'color':     [0.855, 0.145, 0.114, 0.8],
                         "linewidth": 2, "linestyle": ":"}}]
chart.plot_points_dbt_rh(points, connectors)

chart.plot_legend(markerscale=.7, frameon=False, fontsize=10, labelspacing=1.2);
chart.save('mollier_summer.png')
chart.close_fig()

chart = PsychroChart('ashrae')
chart.figure_params['title'] = 'Psychrometric Chart Winter conditions'
chart.figure_params['figsize'] = (11, 8)
zones_conf = {
    "zones": [
        {
            "zone_type": "dbt-rh",
            "style":     {"edgecolor": [0.498, 0.624, 0.8],
                          "facecolor": [0.498, 0.624, 1.0, 0.2],
                          "linewidth": 2,
                          "linestyle": "--"},
            "points_x":  [23, 26],
            "points_y":  [20, 60],
            "label":     ""
        }
    ]}
chart.append_zones(zones_conf)

points = {'outside':     {'label': 'Estimated (Weather service)',
                          'style': {'color':  [0.855, 0.004, 0.278, 0.8],
                                    'marker': 'X', 'markersize': 15},
                          'xy':    (4.73, 49.3)},
          'inlet_100':       {
              'label': 'Inlet < RH',
              'style': {'color':  [0.573, 0.106, 0.318, 0.5],
                        'marker': 'x', 'markersize': 15},
              'xy':    (26.22, 12.25)},
          'inlet': {
              'label': 'Inlet airstream',
              'style': {'color':  [0.573, 0.106, 0.318, 0.5],
                        'marker': 'x', 'markersize': 15},
              'xy':    (26.22, 40.0)},
          'interior':    {'label': 'Interior',
                          'style': {'color':  [0.592, 0.745, 0.051, 0.9],
                                    'marker': 'o', 'markersize': 15},
                          'xy':    (23, 40)}}
connectors = [{'start': 'outside',
               'end':   'inlet_100',
               'style': {'color':     [0.573, 0.106, 0.318, 0.7],
                         "linewidth": 2, "linestyle": "-."}},
              {'start': 'inlet_100',
               'end':   'inlet',
               'style': {'color':     [0.573, 0.106, 0.318, 0.7],
                         "linewidth": 2, "linestyle": "-."}},
              {'start': 'inlet',
               'end':   'interior',
               'style': {'color':     [0.855, 0.145, 0.114, 0.8],
                         "linewidth": 2, "linestyle": ":"}}]
chart.plot_points_dbt_rh(points, connectors)

chart.plot_legend(markerscale=.7, frameon=False, fontsize=10, labelspacing=1.2);
chart.save('mollier_winter.png')
chart.close_fig()
