% !TeX root = MTIreport.tex
% !TeX spellcheck = en-US
\chapter{Heat energy devices}\label{chap:Energy transfer device}
As described in chapter \ref{chap:Thermal comfort}, air temperature, air speed and relative humidity play an important role in creating a comfortable environment. Alteration of air temperature and relative humidity can both be achieved, by either cooling or heating air. A focus of this chapter will therefor be on those two methods. \Citeauthor{ouwehand_toegepaste_2012} and \Citetitle{_jellema_2011} describes a multitude of devices that could be used for climate control. A selection is made on the criteria of costs and general public acceptance. This results in an in depth review of the following devices:
\begin{itemize}
    \item Heat pump
    \item Refrigerator
    \item Solar thermal devices
\end{itemize}

\section{Heat pump}\label{sec:Heat pump}
A heat pump transfers heat energy from a source of heat to a colder destination, also called a "heat sink" The transfer of this thermal energy is counter to the direction of spontaneous heat transfer. This is illustrated in figure \ref{fig:Block diagrams of a heat pump}, which shows that a higher energy \gls{s:Q_H} is obtained by adding additional work \gls{s:W_c} or energy \gls{s:Q_c} to a lower energy \gls{s:Q_L}. Heat is transferred from source to sink, with a \gls{refrigerant}, which undergoes a thermodynamic cycle with phase changes. Both \citeauthor{ouwehand_toegepaste_2012-1, scoccia_absorption_2017, wang_handbook_2000} state that the use of a heat pump can greatly reduce the primary energy need for a system.

\begin{note}
    Refrigerants are discussed in more detail in chapter{chap:Refrigerant}
\end{note}

Several heat pump types exist; some require external mechanical work while others require external thermal energy ~\cite{chua_advances_2010}.~\Citeauthor{ouwehand_toegepaste_2012-1} identifies five of these principles subdivided in mechanical and thermal types; Heat pumps that need a source of external mechanical work are a \acrfull{EHP} and \acrfull{MVR}. While the \acrfull{AHP}, \acrfull{GDHP} and \acrfull{GEHP} rely on the addition of thermal energy. A more detailed description for different type of heat pumps can be found at table \ref{tbl:Heat pump difference}.

\begin{figure}[!htbp]
    \centering
    \begin{tikzpicture}[auto, node distance=2cm,>=latex']
        \node[input, name=input] {};
        \node[block, right of=input, xshift=1cm, fill=heatpump_orange] (heat pump) {Heat pump};
        \node[output, right of=heat pump, xshift=1cm] (output) {};
        \node[input, below of=heat pump] (comp input) {};

        \draw[->] (input) -- node{\begin{tabular}{c}
                $ \gls{s:Q_L} $ \\
                $ \gls{s:T_L} $
            \end{tabular}
        } (heat pump);
        \draw[->] (heat pump) -- node{\begin{tabular}{c}
                $ \gls{s:Q_H} $ \\
                $ \gls{s:T_H} $
            \end{tabular}
        } (output);
        \draw[->] (comp input) -- node{\begin{tabular}{c}
                $ \gls{s:Q_c} $ \\
                $ \gls{s:W_c} $
            \end{tabular}
        } (heat pump);
    \end{tikzpicture}
    \caption{Block diagrams of a general heat pump}\label{fig:Block diagrams of a heat pump}
\end{figure}

\begin{table}[!htbp]
    \begin{tabularx}{\textwidth}{|l | l | X|}
        \hline
        \textbf{Acronym}         & \textbf{Type} & \textbf{Description}                                                                                                                                                                                                                                                               \\ \hline
        \textbf{\acrshort{EHP}}  & Electrical    & Makes use of a mechanical compression cooling device, driven by an electrical engine                                                                                                                                                                                               \\ \hline
        \textbf{\acrshort{MVR}}  & Electrical    & A centrifugal compressor ensures a pressure rise in a saturated vapor, for instance water vapor, which will result in a rise in temperature, resulting in a super heated vapor. These devices are mostly used in industrial processes.                                             \\ \hline
        \textbf{\acrshort{AHP}}  & Thermal       & Is a thermally driven heat pump, based on an absorption cooling device, working with a \gls{refrigerant} and a \gls{solvent}. The machine has a cycle for a refrigerant and a cycle for a solvent. The machine itself is driven with heat from a steam- or hot water installation. \\ \hline
        \textbf{\acrshort{GDHP}} & Thermal       & Is a thermally driven heat pump. A \gls{refrigerant} circulates by natural convention without the use of a pump. There are three different mediums in a hermetically closed system.                                                                                                \\ \hline
        \textbf{\acrshort{GEHP}} & Thermal       & A cooling device that uses mechanical compression, but is regarded as a thermally driven heat pump. This is because the compressor is driven by a combustion engine. An exhaust gas cooler extracts heat from the exhaust gasses and adds that to the heat flow of the heat pump.  \\ \hline
    \end{tabularx}
    \caption{Heat pumps and their basic principles}\label{tbl:Heat pump difference}
\end{table}

\subsection{Operating modes}
\Citeauthor{wang_handbook_2000} explains that heat pumps can operate in two different modes; They can either have a cooling or a heating function. Were the state is toggled if a predetermined set temperature is reached. Switching the reversing valves, basically making the indoor coil act as an evaporator and the outdoor coil act as a condenser, which could also be described as a refrigerator. Described in section \ref{sec:Refrigerator systems}.

\begin{figure}[!htbp]
    \centering
    \includegraphics[height=10cm]{rooftopheatpumpWangWang2000Handbookofairconditioningandrefrigeration.png}
    \caption{Schematic diagram of a typical rooftop heat pump \cite{wang_handbook_2000}}\label{`fig:Schematic diagram of a typical rooftop heat pump`}
\end{figure}

\subsection{Heat pump efficiency}
An important factor to consider, when choosing a heat pump is its efficiency. Expression of this efficiency differs for mechanical and thermally based heat pumps. The equations below are a general accepted way to express the efficiency for a mechanical driven heat pump and are based upon the works of \citeauthor{ouwehand_toegepaste_2012-1}. The workings of this heat pump are expressed in a function diagram, given in figure \ref{fig:Function diagram for a compression based heat pump}.

\begin{figure}[!htbp]
    \centering
    \begin{tikzpicture}[auto, node distance=2cm,>=latex']
        \node[block, name=condensor, fill=heatpump_orange] {
            \centering
            \begin{tabular}{c}
                Condensor \\
                \gls{s:T_H}
            \end{tabular}};
        \coordinate[below of=condensor] (lcond);

        \node[block, left of=lcond, fill=heatpump_orange] (E) {E};
        \node[block, right of=lcond, fill=compressor_blue] (C) {C};
        \node[input, right of=C] (wc) {};

        \node[block, below of=condensor, yshift=-2cm, fill=heatpump_orange] (evaporator) {
            \centering
            \begin{tabular}{c}
                Evaporator \\
                \gls{s:T_L}
            \end{tabular}};
        \draw[->] (condensor.west) -| (E.north);
        \draw[->] (E.south) |- (evaporator.west);
        \draw[->] (evaporator.east) -| (C.south) node[pos=0.8] {\gls{s:Q_L}};
        \draw[->] (C.north) |- (condensor.east) node[pos=0.2] {\gls{s:Q_H}};
        \draw[->] (wc) -- (C.east) node[pos=0.0] {\gls{s:W_c}};

    \end{tikzpicture}
    \caption{Function diagram for a compression based heat pump}\label{fig:Function diagram for a compression based heat pump}
\end{figure}

A heating factor \gls{s:varepsilon_w}, such as the one given in equation \ref{eq:heat factor}, could be viewed as measure for energy conversion. It is the efficiency at which mechanical added energy is converted in to usable heat. \gls{s:varepsilon_w} is also referenced as \acrfull{COP} in the reviewed literature.

\begin{equation}\label{eq:heat factor}
    \glsmath{s:varepsilon_w} = \frac{\glsmath{s:Q_H}}{\glsmath{s:W_c}}
\end{equation}

\noindent When a heat pump has no losses and its cycle could be described as a reversible Carnot cycle, equation \ref{eq:heat factor carnot} can also be expressed in terms of temperature.

\begin{equation}\label{eq:heat factor carnot}
    \glsmath{s:varepsilon_c} = \frac{\glsmath{s:T_H}}{\glsmath{s:T_H} - \glsmath{s:T_L}}
\end{equation}

A real system can better be expressed in a ratio between the enthalpy difference during the evaporation stage. Which occurs between point 2 and 3 and the enthalpy difference between point 1 and 2, which is the compression stage. When this cycle is described in a \textbf{log P h} diagram, such is shown in figure \ref{fig:Log p-h-diagram of the refrigerant R134a}, a quick calculation for a theoretical heat factor \gls{s:varepsilon_th} can be obtained. Which is shown in equation \ref{eq:heat factor theoretical}. The \gls{s:varepsilon_th} differs from \gls{s:varepsilon_c} because the expansion between point 3 and 4 is irreversible.

\begin{figure}[!htbp]
    \includegraphics[width=\textwidth]{r134acycle.png}
    \caption{Log p-h-diagram of the refrigerant R134a}\label{fig:Log p-h-diagram of the refrigerant R134a}
\end{figure}

\begin{equation}\label{eq:heat factor theoretical}
    \glsmath{s:varepsilon_th} = \frac{\glsmath{s:h_2} - \glsmath{s:h_3}}{\glsmath{s:h_2} - \glsmath{s:h_1}}
\end{equation}

\Citeauthor{ouwehand_toegepaste_2012-1} states that there will occur thermodynamical losses, during each phase change. This is results in: $ \gls{s:varepsilon_c} > \gls{s:varepsilon_th} > \gls{s:varepsilon_w} $. These thermodynamical losses amount to a relative big impact and these need to be accounted for. While they further state that the friction loss, which occur due to a moving fluid through pipes, valves and bends, should also be taken into account.  This is contradicted by \Citeauthor{kim_heat_2011}, which states that a pressure drop is negligible in the tubes and heat exchangers. Using a $ dP = 0 $ for his analysis.

An other important impact on a heat factor \gls{s:varepsilon_w} is the efficiency of the electrical motor \gls{s:eta_m}, which are losses that occur in an engine for a conversion of electrical power \gls{s:P_e} to usable power on its axle \gls{s:P_s}. Normal efficiency values are between $ 75 [\%] $ and $ 90 [\%] $ \cite{ouwehand_toegepaste_2012-1}. Equation \ref{eq:motor efficiency} shows how \gls{s:eta_m} can be obtained.

\begin{equation}\label{eq:motor efficiency}
    \glsmath{s:eta_m} = \frac{\glsmath{s:P_s}}{\glsmath{s:P_e}}
\end{equation}

A heat factor is further influenced by losses in the compressor, where a \gls{refrigerant} is pressurized from vapor pressure till condensation pressure. This can be achieved with a centrifugal or displacement compressor. Friction losses in a compressor will result in added enthalpy to the \gls{refrigerant}, which will be used during the condensing phase. This is therefor not regarded as a complete loss of usable energy \cite{ouwehand_toegepaste_2012-1}.

\begin{figure}[!htbp]
    \centering
    \begin{tikzpicture}[auto, node distance=2cm,>=latex']
        \node[input, name=input] {};
        \node[block, right of=input, xshift=1cm, fill=compressor_blue] (heat pump) {Compressor};
        \node[output, right of=heat pump, xshift=1cm] (output) {};
        \node[input, below of=heat pump] (comp input) {};

        \draw[->] (input) -- node{\begin{tabular}{c}
                $ \gls{s:m_flux_rf} $ \\
                $ \gls{s:p_in}, \gls{s:h_in} $
            \end{tabular}
        } (heat pump);
        \draw[->] (heat pump) -- node{\begin{tabular}{c}
                $ \gls{s:m_flux_rf} $ \\
                $ \gls{s:p_out}, \gls{s:h_out} $
            \end{tabular}
        } (output);
        \draw[->] (comp input) -- node{\gls{s:P_eff}} (heat pump);
    \end{tikzpicture}
    \caption{Block diagrams of an electrical driven compressor}\label{fig:Block diagrams of an electrical driven compressor}
\end{figure}

\noindent Figure \ref{fig:Block diagrams of an electrical driven compressor} illustrates the relations between the relevant physical quantities. Where the \glsdesc{s:P_eff} \gls{s:P_eff} is derived from the first thermodynamics law. Resulting in equation \ref{eq:effective compressor power}.

\begin{equation}\label{eq:effective compressor power}
    \glsmath{s:P_eff} = \glsmath{s:m_flux_rf} \left(\glsmath{s:h_out} - \glsmath{s:h_in} \right)
\end{equation}

\noindent A \glsdesc{s:w_eff} \gls{s:w_eff} can be obtained using equation \ref{eq:effective isentropic compressor work}.

\begin{equation}\label{eq:effective isentropic compressor work}
    \glsmath{s:w_eff} = \frac{\glsmath{s:P_eff}}{\glsmath{s:m_flux_rf}} = \glsmath{s:h_out_prime} - \glsmath{s:h_in}
\end{equation}

\noindent Allowing the definition of the isentropic efficiency of the compressor \gls{s:eta_is_comp} in equation \ref{eq:isentropic efficiency compressor}, in which \gls{s:w_is} is the \glsdesc{s:w_is} expressed as the enthalpy difference $ \gls{s:h_out} - \gls{s:h_in} $ \cite{linden_toegepaste_2003, ouwehand_toegepaste_2012, kimmenaede_warmteleer_2010}

\begin{equation}\label{eq:isentropic efficiency compressor}
    \glsmath{s:eta_is_comp} = \frac{\glsmath{s:w_eff}}{\glsmath{s:w_is}} = \frac{\glsmath{s:h_out_prime} - \glsmath{s:h_in}}{\glsmath{s:h_out} - \glsmath{s:h_in}}
\end{equation}

\noindent The total efficiency of a compressor can be expressed as equation \ref{eq:total compressor efficiency}.

\begin{equation}\label{eq:total compressor efficiency}
    \glsmath{s:eta_eff_comp} = \glsmath{s:eta_is_comp} \glsmath{s:eta_m} \gls{s:eta_mech_comp}
\end{equation}

\noindent All relevant losses and energy flows, for a heat pump, can now be visualized in a \gls{Sankey diagram}, such as figure \ref{fig:Sankey diagram showing the energy flow for a heat pump system}.

\begin{figure}[!htbp]
    \centering
    \input{sankeyheatpump.pgf}
    \caption{\gls{Sankey diagram} showing the energy flow for a heat pump system}\label{fig:Sankey diagram showing the energy flow for a heat pump system}
\end{figure}

\section{Refrigerator systems}\label{sec:Refrigerator systems}
The main function of a refrigerator is obtaining and maintaining a lower temperature of an object or fluid mass, compared to its surrounding atmosphere \cite{ouwehand_toegepaste_2012}. The heat energy to be released from an object is called a \gls{cooling load}. \Citeauthor{ouwehand_toegepaste_2012} states that the most common types of refrigerator systems are:
\begin{itemize}
    \item Mechanical compression refrigerator
    \item Absorption refrigerator
    \item Adsorption refrigerator
    \item Expansion refrigerator
    \item Electrothermic refrigerator
\end{itemize}
Mechanical compression and absorption refrigerators find the most common use in climate control appliances. This is confirmed by \Citeauthor{afonso_recent_2006, wang_handbook_2000}, although these sources both describe additional systems; Which are not mentioned by \Citeauthor{ouwehand_toegepaste_2012}. These are mostly hybrid systems between Heat / electricity, solar / biomass and chemical / thermal. Those systems are not yet common use, and will not be discussed in this document.

Refrigerator systems consist of at least a refrigerator, responsible for a change in heat energy of a \gls{refrigerant}, which is a carrier for the heat energy, from the body to be cooled, to a heat sink.

\begin{note}
    Refrigerants are discussed in more detail in chapter \ref{chap:Refrigerant}
\end{note}

\subsection{Refrigerator system efficiency}
A main selection criteria for a refrigerator system, is its effective ability to transfer an amount of heat from a system. This is also called its refrigerating capacity \gls{s:Q_rc}, shown in equation \ref{eq:refrigerating capacity}. Which is a product of a mass flow of \gls{refrigerant} \gls{s:m_flux_rf} and its capability to store a certain amount of heat per mass, as is called its refrigerating effect \gls{s:q_rf} \cite{ouwehand_toegepaste_2012, wang_handbook_2000}.

\begin{equation}\label{eq:refrigerating capacity}
    \glsmath{s:Q_rc} = \glsmath{s:q_rf} \glsmath{s:m_flux_rf}
\end{equation}

The efficiency of the whole system can be expressed as the \glsdesc{s:varepsilon_eff}  \gls{s:varepsilon_eff}, which is the ratio between \gls{s:Q_rc} and \gls{s:P_eff}. Resulting in equation \ref{eq:effective cooling efficiency}. This efficiency does not take into account all energy conversions in to the energy chain, at partial load during different seasons. For this the EU introduced the \acrfull{ESEER} \cite{ouwehand_toegepaste_2012}.

\begin{equation}\label{eq:effective cooling efficiency}
    \glsmath{s:varepsilon_eff} = \frac{\glsmath{s:Q_rc}}{\glsmath{s:P_eff}}
\end{equation}

The energy flow can be visualized in figure \ref{fig:Sankey diagram showing the energy flow for a refrigerator system}, were an external influx of heat working in on the system \gls{s:Q_ext}, due to radiation, is added; Which together with an effective compressor energy \gls{s:P_eff}, results in a net heat flow that the evaporator needs to dispatch \gls{s:Q_con}.

\begin{figure}[!htbp]
    \centering
    \input{sankeyrefrigeratorsystem.pgf}
    \caption{\gls{Sankey diagram} showing the energy flow for a refrigerator system}\label{fig:Sankey diagram showing the energy flow for a refrigerator system}
\end{figure}

\begin{note}
    The detailed workings of a mechanical compression refrigerator could be characterized as a heat pump in reverse, which was described in section \ref{sec:Heat pump} and is therefor omitted from this report, leaving only the absorption refrigerator, which is discussed in section \ref{sec:Absorption}.
\end{note}

\subsection{Absorption refrigerator}\label{sec:Absorption}
The working fluid in an absorption refrigeration system is a binary solution consisting of refrigerant and absorbent. This process is reversible and can occur by heating the mixture. In figure \ref{fig:absorption process}(a), two evacuated vessels are connected to each other. The left vessel contains liquid refrigerant while the right vessel contains a binary solution of absorbent / refrigerant. The solution in the right vessel will absorb refrigerant vapor from the left vessel causing pressure to reduce. While the refrigerant vapor is being absorbed, the temperature of the remaining refrigerant will reduce as a result of its vaporization. This causes a refrigeration effect to occur inside the left vessel. At the same time, solution inside the right vessel becomes more dilute because of the higher content of refrigerant absorbed. This is called the "absorption process" \cite{srikhirin_review_2001}. \Citeauthor{allouhi_solar_2015} states that an absorption refers to the process in which a substance penetrates and gets incorporated into another one of a different state. These two states create a special attraction to form a strong solution called mixture. Normally, the absorption process is an exothermic process, therefore, it must reject heat out to the surrounding in order to maintain its absorption capability \cite{srikhirin_review_2001}.

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.6\textwidth]{Srikhirin_et_al_2001_A_review_of_absorption_refrigeration_technologies_1.png}
    \caption{(a) Absorption process occurs in right vessel causing cooling effect in the other; (b) Refrigerant separation process occurs in the right vessel as a result of additional heat from outside heat source \cite{srikhirin_review_2001}}\label{fig:absorption process}
\end{figure}

Whenever the solution cannot continue with the absorption process because of saturation of the refrigerant, the refrigerant must be separated out from the diluted solution. Heat is normally the key for this separation process. It is applied to the right vessel in order to dry the refrigerant from the solution as shown in figure \ref{fig:absorption process}(b). The refrigerant vapor will be condensed by transferring heat to the surroundings. With these processes, the refrigeration effect can be produced by using heat energy. However, the cooling effect cannot be produced continuously as the process cannot be done simultaneously. Therefore, an absorption refrigeration cycle is a combination of these two processes as shown in figure \ref{fig:absorption refrigeration cycle} As the separation process occurs at a higher pressure than the absorption process, a circulation pump is required to circulate the solution \cite{srikhirin_review_2001}.

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.6\textwidth]{Srikhirin_et_al_2001_A_review_of_absorption_refrigeration_technologies_2.png}
    \caption{A continuous absorption refrigeration cycle composes of two processes mentioned in the earlier
        figure\cite{srikhirin_review_2001}}\label{fig:absorption refrigeration cycle}
\end{figure}

The most popular fluids in the absorption system are \ch{H2O} \& \ch{LiBr} (water as refrigerant and lithium bromide as secondary fluid) and \ch{NH3} \& \ch{H2O} (ammonia as refrigerant and water as secondary fluid) \cite{srikhirin_review_2001, afonso_recent_2006}. The first pair of fluids are suitable for positive temperatures in the evaporator while the second one can also be used for negative temperatures. However, the \ch{NH3} \& \ch{H2O} systems are not very common, due to their low efficiency (average cooling \acrshort{COP} = 0.6), high heat transfer areas and initial cost. Research is being carried out in order do develop different pairs \cite{afonso_recent_2006}.

\section{Solar thermal devices}
Solar energy is the most common type of renewable energy presently being used by most of the countries. Low price and cleanliness are some advantages of this type of energy \cite{afshar_review_2012}. There are many usages of solar energy; among them one of the
most potential applications of solar energy is in the form of solar air heaters. Solar air heaters are used for many purposes, e.g. drying agricultural, textile and marine products, and heating of buildings to maintain a comfortable environment \cite{shukla_state_2012}. But \Citeauthor{raisul_islam_solar_2013} tells us that the most widely solar thermal applications is the solar water heating system \cite{raisul_islam_solar_2013}. The solar water heating system uses natural solar thermal technology \cite{yan_simplified_2015} which is where solar radiation is converted into heat and transmitted into a transfer medium such as water, water antifreeze or air. This system is often feasible for replacement of electricity and fossil fuels used for water heating \cite{sivakumar_performance_2012}. Generally, the system is very simple because it requires only sunlight to heat the water. It works when the working fluid is brought in contact with a dark surface (high absorptive) that is exposed to sunlight, which then causes the temperature of the fluid to rise \cite{al-badi_domestic_2012}. This fluid may be caused by the water being heated directly, which is called a direct system, or it may be a heat transfer fluid (such as a glycol or water mixture) that is passed through some form of a heat exchanger and called an indirect system. These systems can be classified into two main categories; active system and passive system. The active system can be divided into two which are the open loop and close loop system whereas the passive system uses the system of thermosiphon and \acrfull{ICS} \cite{jamar_review_2016}.

Generally, the solar water heater consists of several basic components; the solar radiation collector panel, storage tank and heat transfer fluid \cite{kumar_thermal_2010}, as shown in figure \ref{fig:Components of the solar water heater}. Besides, there are also several additional components such as the pump (necessary in active system only), auxiliary heating unit, piping units and heat exchanger.

\begin{figure}[!htbp]
    \centering
    \begin{tikzpicture}[auto, node distance=2cm,>=latex']
        \node[block, name=system] {System};
        \node[block, below of=system] (storage tank) {Storage Tank};
        \node[block, left of=storage tank] (fluid) {Fluid};
        \node[block, right of=storage tank] (collector) {Collector};
        \node[block, below of=collector] (etc) {\acrshort{ETC}};
        \node[block, right of=etc] (fpc) {\acrshort{FPC}};
        \node[block, left of=etc] (pdr) {\acrshort{PDR}};
        \node[block, left of=pdr] (ptc) {\acrshort{PTC}};

        \draw[->] (system) -- (fluid);
        \draw[->] (system) -- (storage tank);
        \draw[->] (system) -- (collector);
        \draw[->] (collector) -- (etc);
        \draw[->] (collector) -- (fpc);
        \draw[->] (collector) -- (pdr);
        \draw[->] (collector) -- (ptc);
    \end{tikzpicture}
    \caption{Components of the solar water heater}\label{fig:Components of the solar water heater}
\end{figure}

Firstly, the major component of solar water heater is the solar collector \cite{krishnavel_experimental_2014}. It is a special kind of heat exchanger that transforms the solar radiation energy to internal energy of the transport medium \cite{garcia_experimental_2013} in which it absorbs the incoming solar radiation, and converts it into heat, then transfers the heat to a fluid (usually air, water, or oil) flowing through the collector \cite{hossain_review_2011}.

There are mainly three types of solar collectors \cite{chong_study_2012} that are used in solar water heating systems such as \acrfull{FPC}, \acrfull{ETC} and concentrated solar collectors such as \acrfull{PDR} and \acrfull{PTC} \cite{hossain_review_2011}. \acrshort{FPC} shown in figure \ref{fig:fpc} is one of the types of stationary collectors \cite{tian_review_2013} and also the major component of any solar water heating system \cite{jaisankar_comprehensive_2011}. It is also a popular collector device \cite{mahian_performance_2014}.

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.6\textwidth]{fpc.png}
    \caption{\acrfull{FPC} \cite{jamar_review_2016}}\label{fig:fpc}
\end{figure}

A simple heat balance for a \acrshort{FPC} can be expressed as equation \ref{eq:heat balance fpc} \cite{ouwehand_toegepaste_2012-1}. Where \gls{s:A_c} is the \glsdesc{s:A_c}. \gls{s:G_T} is the \glsdesc{s:G_T} and is usually value between $  850 ... 1000 \left[\glsunit{s:G_T}\right] $ \cite{ouwehand_toegepaste_2012-1}. \gls{s:Q_n} is the actual \glsdesc{s:Q_n}. The other terms are indexed with the following convention \ensuremath{\dot{Q}_{n,m}}, where $ n $ is either a heat flow due to $ s $ radiation, $ r $ reflection, $ c $ convection and where $ m $ is $ ab $ for absorber,  $ cp $ for cover plate or $ co $ for complete cover.

\begin{equation}\label{eq:heat balance fpc}
    \glsmath{s:A_c} \glsmath{s:G_T} = \glsmath{s:Q_n} + \glsmath{s:Q_s_ab} + \glsmath{s:Q_r_ab} + \glsmath{s:Q_c_ab} + \glsmath{s:Q_s_cp} + \glsmath{s:Q_r_cp} + \glsmath{s:Q_c_cp} + \glsmath{s:Q_c_co}
\end{equation}

\begin{equation}\label{eq:energy gain}
    \glsmath{s:Q_n} = \glsmath{s:m_flux} \glsmath{s:c_p} \left(\glsmath{s:T_out} - \glsmath{s:T_in}\right)
\end{equation}

\noindent Normally the efficiency can be calculated using equation \ref{eq:efficiency solar collector}, where the useful energy is set against the incoming solar radiation for the total collector surface.

\begin{equation}\label{eq:efficiency solar collector}
    \glsmath{s:eta_c} = \frac{\glsmath{s:Q_n}}{\glsmath{s:A_c} \glsmath{s:G_T}}
\end{equation}

Equation \ref{eq:heat balance fpc} could be substituted in equation \ref{eq:efficiency solar collector}, but the losses would be hard to quantify. A better approach would be to determine the losses with a statistic interpretation of measurements. This is done according EN-12975-2, from which equation \ref{eq:efficiency solar collector statitical testmethod} is obtained. Here \gls{s:tau} is \glsdesc{s:tau}. \gls{s:alpha} is \glsdesc{s:alpha} and \gls{s:k_1} is \glsdesc{s:k_1}, whilst \gls{s:k_2} is \glsdesc{s:k_2}. Together with the $ \Delta T $ between the \glsdesc{s:T_m} \gls{s:T_m} and \glsdesc{s:T_a} over the \glsdesc{s:G_T} \gls{s:G_T}. Tells us the efficiency.

\begin{equation}\label{eq:efficiency solar collector statitical testmethod}
    \glsmath{s:eta_c} = \glsmath{s:tau} \glsmath{s:alpha} - \glsmath{s:k_1} \frac{\glsmath{s:T_m} - \glsmath{s:T_a}}{\glsmath{s:G_T}} - \glsmath{s:k_2} \frac{\left(\glsmath{s:T_m} - \glsmath{s:T_a}\right)^2}{\glsmath{s:G_T}}
\end{equation}

\begin{note}
    \gls{s:k_1} \& \gls{s:k_2} are given for each of-the-shell collector.
\end{note}

\subsection{Operating modes}
A solar water heater can also be used for refrigerating. This can be achieved using three different principles \acrfull{DEC} and absorption or adsorption refrigeration \cite{ouwehand_toegepaste_2012-1}. \acrshort{DEC} cooling involves a combination of dehumidification and evaporative cooling processes. It is considered as an open cycle sorption cooling system because sorbent is used to dehumidify air \cite{allouhi_solar_2015}. While adsorption refrigeration cycle relies on the adsorption of a refrigerant gas on an adsorbent at low pressure and subsequent desorption by heating the adsorbent. It acts as a chemical compressor driven by heat \cite{critoph_rapid_1999}.

The focus of this review will be on absorption refrigeration, because it is a wide accepted technology in climate control, for buildings \cite{zhai_review_2011}. Absorption refrigeration was reviewed in detail in section \ref{sec:Absorption}. A solar-operated absorption refrigeration system is made of a solar collector and a refrigeration cycle as shown in \ref{fig:Solar absorption refrigerator}. The system differs from a customary fossil-fuel-fired unit in that the energy supplied to the generator is directly from the solar collector. This arrangement has the advantage of making high temperature thermal energy available to the generator \cite{fathi_irreversible_2004}.

Flat-plate solar collectors are commonly used in solar space heating. It is economically sound when the same collector is used for both space heating and cooling. However, because of the relatively low temperatures attainable when the flat-plate collectors are used, only a few practical methods are available in flat-plate solar-operated cooling processes. One of the most promising schemes is the utilization of an absorption refrigeration cycle with solar energy serving as the source of heat to operate the generator \cite{fathi_irreversible_2004}.

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.5\textwidth]{solarfridge.png}
    \caption{Solar absorption refrigerator \cite{fathi_irreversible_2004}}\label{fig:Solar absorption refrigerator}
\end{figure}

\subsection{Solar absorption refrigerator efficiency}
The performance of a solar absorption refrigerator is usually estimated by first calculating the maximum COP that can be expected from an ideal system as shown in Fig. 2. The overall COP glsbeta of a solar absorption refrigerator is the product of the efficiency of the solar collector glseta and the COP of the absorption refrigerator \cite{fathi_irreversible_2004}.
