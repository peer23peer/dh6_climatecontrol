import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
from matplotlib.sankey import Sankey


def sum_tuple(a, b):
    return tuple([sum(x) for x in zip(a, b)])


fig = plt.figure(figsize=(5.73, 4.07), dpi=100)
ax = fig.add_subplot(1, 1, 1, xticks=[], yticks=[])
sankey = Sankey(ax=ax, unit=None, gap=0.1)
sankey.add(flows=[0.5, 0.45, 0.05, -0.1, -0.1, -0.8],
           pathlengths=[1.75, 0.5, 0.15, 0.15, 0.15, 0.25],
           labels=['$ Q_{L} $', r'$ P_{eff}\ \ \ Q_{C} $', '', r'$ \Sigma Q (1 - \varepsilon_{th}) $',
                   r'$ \Sigma Q (1 - \varepsilon_{w} ) $', r'$ Q_{H} $'],
           label='Heat energy',
           trunklength=0.5,
           orientations=[0, 0, -1, 1, 1, 0],
           facecolor='#ff9900')
sankey.add(flows=[0.75, -0.45, -0.1, -0.1, -0.1],
           label='Compressor energy',
           labels=['$ P_{elec} $', '', '', r'$ P_{elec} (1 - \eta_{mech}) $', '$ P_{elec} ( 1 - \eta_m )$'],
           orientations=[0, 0, -1, -1, -1],
           pathlengths=[0.12, 0.1, 0.15, 0.5, 0.5],
           prior=0,
           connect=(1, 1),
           facecolor='#0066ff',
           hatch='')
sankey.add(flows=[0.1, -0.05, -0.05],
           pathlengths=[0.15, 0.1, 0.11],
           labels=['', '', ''],
           trunklength=0.15,
           orientations=[0, 1, 0],
           prior=1,
           connect=(2, 0),
           facecolor='#0066ff',
           hatch='')
sankey.add(flows=[0.05, -0.05],
           orientations=[0, 1],
           prior=2,
           trunklength=0.6,
           connect=(1, 0),
           facecolor='#0066ff',
           hatch='')
ax.text(0.38, 0.19, r'$ P_{elec} (1 - \eta_{is,comp}) $', transform=ax.transAxes, fontsize=8)
ax.text(0.43, 0.235, r"$ h_{out'} - h_{out} $", transform=ax.transAxes, fontsize=8)
diagrams = sankey.finish()
for d in diagrams:
    for t in d.texts:
        t.set_fontsize(8)
diagrams[0].texts[0].set_fontsize(16)
diagrams[0].texts[0].set_weight('black')
diagrams[0].texts[1].set_fontsize(16)
diagrams[0].texts[1].set_weight('black')
diagrams[0].texts[-1].set_fontsize(16)
diagrams[0].texts[-1].set_weight('black')
diagrams[1].texts[0].set_fontsize(16)
diagrams[1].texts[0].set_weight('black')

diagrams[0].texts[4].set_position(sum_tuple(diagrams[0].texts[4].get_position(), (0.15, -0.1)))
diagrams[0].texts[3].set_position(sum_tuple(diagrams[0].texts[3].get_position(), (0., -0.05)))
diagrams[1].texts[0].set_position(sum_tuple(diagrams[1].texts[0].get_position(), (-0.1, 0.)))
diagrams[1].texts[3].set_position(sum_tuple(diagrams[1].texts[3].get_position(), (0.25, 0.025)))
diagrams[1].texts[4].set_position(sum_tuple(diagrams[1].texts[4].get_position(), (0., 0.04)))

plt.legend(loc='lower right')
# plt.show()
plt.savefig(r'./report/sankeyheatpump.pgf', dpi=600, bbox_inches='tight',
            orientation='portrait', papertype='a5')
print('finished!')
