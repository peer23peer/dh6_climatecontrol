import networkx as nx
import matplotlib.pyplot as plt
G = nx.DiGraph()

G.add_node("System")
G.add_node("Collector")
G.add_node("Storage tank")
G.add_node("Fluid")
G.add_node("FPC")
G.add_node("ETC")
G.add_node("PDR")
G.add_node("PTC")
G.add_edge("System", "Collector")
G.add_edge("System", "Storage tank")
G.add_edge("System", "Fluid")
G.add_edge("Collector", "FPC")
G.add_edge("Collector", "ETC")
G.add_edge("Collector", "PDR")
G.add_edge("Collector", "PTC")

nx.draw(G, node_color=['green', 'blue', 'blue', 'blue', 'red', 'red', 'red', 'red'] ,with_labels=True)
plt.show()