import MTIpython as mti

u = mti.u

air = [mti.material.material_factory['air_humid'] for m in range(3)]
air[0].T = 26.3 * u.degC
air[2].T = 10.03 * u.degC
air[1].T = (air[0].T.to('K') + air[2].T.to('K')) / 2
for m in air:
    mti.core.binder.lock(m.T)
air[0].RH = 0.493
air[1].W = air[0].W
print('rho air_avg = {}'.format(air[1].rho))

V = 4.8 * u.m ** 3 / u.s
h_cool = 50. * u.kJ / u.kg
P_cool = air[1].rho * V * h_cool
P_cool.ito('kW')
print('Power needed to cool the air = {}'.format(P_cool))

eta_evap = 0.7
h_1 = 1436.5 * u.kJ / u.kg
h_2 = 1807.6 * u.kJ / u.kg
h_4 = 434.8 * u.kJ / u.kg
m_flow_ref = P_cool / ((h_1 - h_4) * eta_evap)
print('Mass flow refrigerant = {}'.format(m_flow_ref.to('kg/s')))

w_t = h_2 - h_1
P_comp = m_flow_ref * w_t
print('Compressor power = {}'.format(P_comp.to('kW')))

epsilon_w = (h_1 - h_4)/(h_2 - h_1)
print('COP = {}'.format(epsilon_w))
