from MTIpython.units.SI import u

from dill import load
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.colors as mplc
import os

#mpl.use("pgf")
pgf_with_custom_preamble = {
    "font.family": "lato",  # use serif/main font for text elements
    "text.usetex": True,  # use inline math for ticks
    "pgf.rcfonts": False,  # don't setup fonts from rc parameters
    "pgf.preamble": [
        "\\usepackage{units}",  # load additional packages
        "\\usepackage{metalogo}",
        "\\usepackage{unicode-math}"
    ]
}

mpl.rcParams.update(pgf_with_custom_preamble)
# mpl.rcParams['text.usetex'] = True
# mpl.rcParams['text.latex.unicode'] = True

fpath = r'D:\Projects\han\ES_climatecontrol\load_calc\data\results_2014_2017_rev1.sim'
fweather = r'D:\Projects\han\ES_climatecontrol\load_calc\data\history_2010_2017.wet'
save_path = r'D:\Projects\han\ES_climatecontrol\calculations\resources'

def get_cmap(data, catcol='outside.T'):
    minima = data[catcol].min()
    maxima = data[catcol].max()
    norm = mplc.Normalize(vmin=minima, vmax=maxima, clip=True)
    mapper = cm.ScalarMappable(norm=norm, cmap=cm.gnuplot)
    cmap = []

    for v in data[catcol]:
        cmap.append(mapper.to_rgba(v))

    mapper.set_array(norm)
    return cmap, mapper


with open(fpath, 'rb') as f:
    data: pd.DataFrame = pd.DataFrame.from_dict(load(f))
data = data.set_index(pd.DatetimeIndex(data['time']))
data = data[data.index.dayofweek < 5]
data = data.between_time('8:00', '22:00')
for k in data.keys():
    if k[-2:] == '.T':
        data[k] = data[k]
    if k[-2:] == '.q' or k[-4:] == '.q_c':
        data[k] = data[k] / 1000
data['outside.T'] = data['roof_0.T']
#data['coolload.q'] = -data['coolload.q']
data['season'] = [(date.month % 12 + 3) // 3 for date in data['time']]
data['heat from aula to hall'] = data['source_door_0.q'] + data['source_wallinside_0.q']
data['heat from aula to canteen'] = data['source_door_6.q'] + data['source_wallinside_6.q'] + data[
    'source_wallinside_7.q'] + data['source_wallinside_5.q']
data['heat from aula to ground'] = data['source_floor.q']
data['heat from aula to outside - through roof'] = data['source_roof.q']
data['heat from aula to outside - through walls'] = data['source_outside_wall_0.q'] + data['source_outside_wall_1.q'] +\
                                                    data['source_outside_wall_2.q'] + data['source_outside_wall_3.q'] +\
                                                    data['source_outside_wall_4.q'] + data['source_outside_wall_5.q'] +\
                                                    data['source_window_0.q'] + data['source_window_2.q'] + data[
                                                        'n0_window_sun.q'] + data['n2_window_sun.q']

hb_inside = data[['heat from aula to hall', 'heat from aula to canteen']].resample('M').sum()
hb_inside['sum'] = hb_inside.sum(axis=1)

hb_outside = data[['heat from aula to ground', 'heat from aula to outside - through roof',
                   'heat from aula to outside - through walls']].resample('M').sum()
hb_outside['sum'] = hb_outside.sum(axis=1)

keys = list(data.keys())

# # # Coolload compared with outside temp
# cmap_temp, mapper = get_cmap(data, 'outside.T')
# fig = plt.figure(figsize=(8, 4))
# ax = fig.add_subplot(1, 1, 1)
# data.plot(kind='scatter', x='outside.T', y='coolload.q', legend=True, c=cmap_temp, ax=ax)
# cb = plt.colorbar(mappable=mapper, ax=ax)
# cb.set_label('Temperature in $ [^\circ C] $')
# plt.xlabel('Outside temperature in $ [^\circ C] $')
# plt.ylabel('Heat flow in $ [kW] $')
# fig.tight_layout()
# fig.savefig(save_path + r'\coollingload_vs_outside_temp.png', bbox_inches='tight')
# plt.close(fig)
#
# # Coolload compared against room occupancy
# cmap_temp, mapper = get_cmap(data.loc[data['persons.q'] > 0], 'outside.T')
# fig = plt.figure(figsize=(8, 4))
# ax = fig.add_subplot(1, 1, 1)
# data.loc[data['persons.q'] > 0].plot(kind='scatter', x='outside.T', y='coolload.q', legend=True, c=cmap_temp, ax=ax)
# cb = plt.colorbar(mappable=mapper, ax=ax)
# cb.set_label('Temperature in $ [^\circ C] $')
# plt.xlabel('Generated heat by persons $ [kW] $')
# plt.ylabel('Heat flow in $ [kW] $')
# fig.tight_layout()
# fig.savefig(save_path + r'\coollingload_vs_person_heat.png', bbox_inches='tight')
# plt.close(fig)

# Coolload per season
fig = plt.figure(figsize=(6, 6))
ax1 = fig.add_subplot(2, 2, 1)
ax1.set_title('Spring')
ax1.set_xlabel('Heat flow in $ [kW] $')
ax2 = fig.add_subplot(2, 2, 2, sharex=ax1, sharey=ax1)
ax2.set_title('Summer')
ax2.set_xlabel('Heat flow in $ [kW] $')
ax3 = fig.add_subplot(2, 2, 3, sharex=ax1, sharey=ax1)
ax3.set_title('Autumn')
ax3.set_xlabel('Heat flow in $ [kW] $')
ax4 = fig.add_subplot(2, 2, 4, sharex=ax1, sharey=ax1)
ax4.set_title('Winter')
ax4.set_xlabel('Heat flow in $ [kW] $')
data.loc[data['season'] == 2]['coolload.q'].hist(ax=ax1, color='k', alpha=0.5, bins=50)
data.loc[data['season'] == 3]['coolload.q'].hist(ax=ax2, color='k', alpha=0.5, bins=50)
data.loc[data['season'] == 4]['coolload.q'].hist(ax=ax3, color='k', alpha=0.5, bins=50)
data.loc[data['season'] == 1]['coolload.q'].hist(ax=ax4, color='k', alpha=0.5, bins=50)
ax1.axvline(data.loc[(data['season'] == 2) & (data['coolload.q'] > 0)]['coolload.q'].mean(), color='r', linestyle='dashed', linewidth=1)
ax1.axvline(data.loc[(data['season'] == 2) & (data['coolload.q'] < 0)]['coolload.q'].mean(), color='b', linestyle='dashed', linewidth=1)
ax2.axvline(data.loc[(data['season'] == 3) & (data['coolload.q'] > 0)]['coolload.q'].mean(), color='r', linestyle='dashed', linewidth=1)
ax2.axvline(data.loc[(data['season'] == 3) & (data['coolload.q'] < 0)]['coolload.q'].mean(), color='b', linestyle='dashed', linewidth=1)
ax3.axvline(data.loc[(data['season'] == 4) & (data['coolload.q'] > 0)]['coolload.q'].mean(), color='r', linestyle='dashed', linewidth=1)
ax3.axvline(data.loc[(data['season'] == 4) & (data['coolload.q'] < 0)]['coolload.q'].mean(), color='b', linestyle='dashed', linewidth=1)
ax4.axvline(data.loc[(data['season'] == 1) & (data['coolload.q'] > 0)]['coolload.q'].mean(), color='r', linestyle='dashed', linewidth=1)
ax4.axvline(data.loc[(data['season'] == 1) & (data['coolload.q'] < 0)]['coolload.q'].mean(), color='b', linestyle='dashed', linewidth=1)
fig.tight_layout()
fig.savefig(save_path + r'\coollingload_per_season.png', bbox_inches='tight')
plt.close(fig)

# When to heat with assumed occupation
fig, axes = plt.subplots(1, 1)
axes.set_xlabel('Time $ [hr] $')
to_timestamp = np.vectorize(lambda x: (x.hour))
heat = pd.DataFrame()
heat['Heat'] = pd.Series(to_timestamp(pd.Series(data.loc[data['coolload.q'] > 0].index.time)))
heat['Cool'] = pd.Series(to_timestamp(pd.Series(data.loc[data['coolload.q'] < 0].index.time)))
axes.hist([heat['Heat'], heat['Cool']], label=['Heat', 'Cool'], alpha=0.25, bins=14, stacked=True, color=['r', 'b'])
axes.grid(True)
fig.tight_layout()
fig.savefig(save_path + r'\When_to_heat.png', bbox_inches='tight')
plt.close(fig)

fig, axes = plt.subplots(4, 1, sharex=True)
hb_outside.plot.line(subplots=True, grid=True,
                     legend=False, color='k', ax=axes)
axes[0].set_title('Heat to ground')
axes[1].set_title('Heat to roof')
axes[2].set_title('Heat to Wall')
axes[3].set_title('Outside heat sum')
[ax.set_ylabel(r' $ \left[\frac{kW}{month}\right] $ ') for ax in plt.gcf().axes]
[ax.grid(True) for ax in plt.gcf().axes]
fig.tight_layout()
fig.savefig(save_path + r'\heat_transfer_to_outside.png',  bbox_inches='tight')
plt.close(fig)

fig, axes = plt.subplots(3, 1, sharex=True)
hb_inside.plot.line(subplots=True, grid=True,
                    legend=False, color='k', ax=axes)
axes[0].set_title('Heat to canteen')
axes[1].set_title('Heat to hall')
axes[2].set_title('Inside heat sum')
[ax.set_ylabel(r' $ \left[\frac{kW}{month}\right] $ ') for ax in plt.gcf().axes]
[ax.grid(True) for ax in plt.gcf().axes]
fig.tight_layout()
fig.savefig(save_path + r'\heat_transfer_to_inside.png', bbox_inches='tight')
plt.close(fig)

# fig, axes = plt.subplots(4, 1, sharex=True)
# fig.set_figheight(11)
# fig.set_figwidth(8)
# hb_outside.loc[hb_outside.index.year == 2017].plot.line(subplots=True, grid=True,
#                                                         title="Heat transfer to outside over time (2017) ",
#                                                         legend=False, color='k', ax=axes)
# axes[0].set_title('Heat to ground')
# axes[1].set_title('Heat to roof')
# axes[2].set_title('Heat to Wall')
# axes[3].set_title('Outside heat sum')
# [ax.set_ylabel(r' $ \left[\frac{kW}{month}\right] $ ') for ax in plt.gcf().axes]
# plt.savefig(os.path.join(save_path, r'\heat_transfer_to_outside_2017.png'), dpi=300)
# plt.show()
#
# fig, axes = plt.subplots(3, 1, sharex=True)
# fig.set_figheight(11)
# fig.set_figwidth(8)
# hb_inside.loc[hb_inside.index.year == 2017].plot.line(subplots=True, grid=True,
#                                                       title="Heat transfer to inside compartments over time (2017) ",
#                                                       legend=False, color='k', ax=axes)
# axes[0].set_title('Heat to canteen')
# axes[1].set_title('Heat to hall')
# axes[2].set_title('Inside heat sum')
# [ax.set_ylabel(r' $ \left[\frac{kW}{month}\right] $ ') for ax in plt.gcf().axes]
# plt.savefig(os.path.join(save_path, r'\heat_transfer_to_inside_2017.png'), dpi=300)
# plt.show()
