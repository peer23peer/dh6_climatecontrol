import numpy as np

T = np.array([15.48, 26.22])
RH = np.array([0.5, 0.4])

p_wd_max = 813.* np.exp(T / 17.6) - 200.
p_wd = RH * p_wd_max
x = (0.622 * p_wd) / (1.01325e5 - p_wd)
h = (1.005 * T + x * (2491 + 1.926 * T)) * 1000
T_wb = 27.8 * np.log((h + 36000.) / 45500.)
print(T_wb)
