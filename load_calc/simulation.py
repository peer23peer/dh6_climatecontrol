from MTIpython.units.SI import u
from MTIpython.thermo.NEN5067 import P_p, w_p, Sex, clothing, Q_zg, Orientation, s_zv, s_zd_tot

from datetime import datetime
import numpy as np
from copy import deepcopy
from random import gauss

__all__ = ['simulate_light', 'simulate_occupation', 'simulate_sun', 'simulate_temperature_canteen',
           'simulate_temperature_ground', 'simulate_temperature_hall', 'simulate_aula_T_set', 'simulate_coolload_set']


def simulate_light(dates, surface):
    k_v = 20 * 16 / (4 * (20 + 16))
    N_l = (500 * surface.m) / (0.76 * 0.73 * 0.7 * k_v) * 0.3 * 0.3
    light_q = np.zeros(dates.shape) * u.W
    for i, d in enumerate(dates):
        if d.weekday() < 5 and d.hour > 10 and d.hour < 22:
            light_q[i] = N_l * u.W
    return light_q


def simulate_aula_T_set(dates):
    aula_T = np.ones(dates.shape) * 16. * u.degC
    for i, d in enumerate(dates):
        if d.weekday() < 5 and d.hour > 8 and d.hour < 22:
            if 4 < d.month < 10:
                aula_T[i] = 22. * u.degC
            else:
                aula_T[i] = 20. * u.degC
    return aula_T


def simulate_coolload_set(dates):
    coolload = np.ones(dates.shape) * 20 * u.kW
    for i, d in enumerate(dates):
        if d.weekday() < 5 and d.hour > 8 and d.hour < 22:
            if 4 < d.month < 10:
                coolload[i] = 30. * u.kW
            else:
                coolload[i] = 20. * u.kW
    return coolload


def simulate_occupation(dates, no_of_males, no_of_females, T, T_set, metabolisme):
    T_day_avg = np.zeros(T.shape) * u.degC
    days = (dates[-1] - dates[0]).days
    i = 0
    for d in range(1, days + 1):
        hour_offset = dates[i].hour
        start_h = i + 8 - hour_offset
        end_h = i + 18 - hour_offset
        sum_day_T = sum(T[start_h:end_h].to(u.delta_degC))
        avg_day_T = (sum_day_T / 10).to(u.degC)
        T_day_avg[i:i + 24] = avg_day_T
        i += 24

    person_q = np.zeros(dates.shape) * u.W
    person_wp = np.zeros(dates.shape) * u.mgr / u.s
    for i, d in enumerate(dates):
        if d.weekday() < 5 and (10 < d.hour <= 11 or 14 < d.hour <= 15 or 18 < d.hour <= 20):
            nmale = gauss(no_of_males, 50.)
            nfem = gauss(no_of_females, 20.)
            cloth_fem = clothing(T_day_avg[i], Sex.FEMALE)
            cloth_male = clothing(T_day_avg[i], Sex.MALE)
            person_q[i] = nmale * P_p(cloth_fem, T_set[i], metabolisme)
            person_q[i] += nmale * P_p(cloth_male, T_set[i], metabolisme)
            person_wp[i] = nfem * w_p(cloth_fem, T_set[i], metabolisme)
            person_wp[i] += nfem * w_p(cloth_male, T_set[i], metabolisme)
    return person_q, person_wp


def simulate_temperature_ground(dates, T):
    ground_T = deepcopy(T.m)
    cumsum = np.cumsum(np.insert(T.to('degC').m, 0, 0))
    ground_T[29:] = (cumsum[30:] - cumsum[:-30]) / 30.
    ground_T[:30] = np.cumsum(np.insert(ground_T[:29], 0, 0))
    for i in range(1, 30):
        ground_T[i] /= float(i)
    ground_T[0] = T[0].m
    return (ground_T - 4.) * u.degC


def simulate_temperature_canteen(dates):
    canteen_T = np.ones(dates.shape) * 16. * u.degC
    for i, d in enumerate(dates):
        if d.weekday() < 5 and d.hour > 12 and d.hour < 22:
            if 4 < d.month < 10:
                canteen_T[i] = 22. * u.degC
            else:
                canteen_T[i] = 20. * u.degC
    return canteen_T


def simulate_temperature_hall(dates):
    canteen_T = np.ones(dates.shape) * 16. * u.degC
    for i, d in enumerate(dates):
        if d.weekday() < 5 and d.hour > 8 and d.hour < 22:
            if 4 < d.month < 10:
                canteen_T[i] = 21. * u.degC
            else:
                canteen_T[i] = 19. * u.degC
    return canteen_T


def simulate_sun(dates, q_ze, orientation, A, A_w):
    q_sun = np.zeros(q_ze.shape) * u.W

    # Get the q_ze_max
    q_ze_max = np.zeros(q_ze.shape) * u.W / u.m ** 2
    for y in range(dates[0].year, dates[-1].year + 1):
        for m in range(min(dates).month, max(dates).month + 1):
            day_q_max = np.array([[d, q] for d, q in zip(dates, q_ze) if d.month == m])
            for d in range(min(day_q_max[:, 0]).day, max(day_q_max[:, 0]).day + 1):
                day_max = max(
                    day_q_max[np.where(np.array([True if day.day == d else False for day in day_q_max[:, 0]]))][:, 1])
                q_ze_max[np.where(np.array(
                    [True if day.day == d and day.month == m and day.year == y else False for day in
                     dates[:]]))] = day_max

    # Get the r_ht
    tbl_13a_N = np.zeros((13, 24))
    tbl_13a_N[:5, 7] = 0.768
    tbl_13a_N[:5, 8:18] = 1.
    tbl_13a_N[:5, 18] = 0.767
    tbl_13a_N[:5, 19] = 0.861
    tbl_13a_N[6, 7] = 0.806
    tbl_13a_N[6, 8:18] = 1.
    tbl_13a_N[6, 18] = 0.772
    tbl_13a_N[6, 19] = 0.89
    tbl_13a_N[7, 7] = 0.794
    tbl_13a_N[7, 8:18] = 1.
    tbl_13a_N[7, 18] = 0.767
    tbl_13a_N[7, 19] = 0.861
    tbl_13a_N[8, 7] = 0.757
    tbl_13a_N[8, 8:18] = 1.
    tbl_13a_N[8, 18] = 0.884
    tbl_13a_N[8, 19] = 0.884
    tbl_13a_N[9:, 7:19] = 1.

    tbl_13a_W = np.zeros((13, 24))
    tbl_13a_W[:5, 7:13] = 1.
    tbl_13a_W[:5, 13] = 0.807
    tbl_13a_W[:5, 14] = 0.901
    tbl_13a_W[:5, 15] = 0.979
    tbl_13a_W[:5, 16] = 1.001
    tbl_13a_W[:5, 17] = 1.007
    tbl_13a_W[:5, 18] = 1.009
    tbl_13a_W[:5, 19] = 1.009
    tbl_13a_W[6, 7:13] = 1.
    tbl_13a_W[6, 13] = 0.816
    tbl_13a_W[6, 14] = 0.907
    tbl_13a_W[6, 15] = 0.977
    tbl_13a_W[6, 16] = 1.
    tbl_13a_W[6, 17] = 1.007
    tbl_13a_W[6, 18] = 1.008
    tbl_13a_W[6, 19] = 1.008
    tbl_13a_W[7, 7:13] = 1.
    tbl_13a_W[7, 13] = 0.815
    tbl_13a_W[7, 14] = 0.909
    tbl_13a_W[7, 15] = 0.978
    tbl_13a_W[7, 16] = 1.
    tbl_13a_W[7, 17] = 1.007
    tbl_13a_W[7, 18] = 1.008
    tbl_13a_W[7, 19] = 1.009
    tbl_13a_W[8, 7:13] = 1.
    tbl_13a_W[8, 13] = 0.807
    tbl_13a_W[8, 14] = 0.914
    tbl_13a_W[8, 15] = 0.982
    tbl_13a_W[8, 16] = 1.002
    tbl_13a_W[8, 17] = 1.008
    tbl_13a_W[8, 18] = 1.009
    tbl_13a_W[8, 19] = 1.009
    tbl_13a_W[9:, 7:13] = 1.
    tbl_13a_W[9:, 13] = 0.789
    tbl_13a_W[9:, 14] = 0.914
    tbl_13a_W[9:, 15] = 0.984
    tbl_13a_W[9:, 16] = 1.004
    tbl_13a_W[9:, 17] = 1.008
    tbl_13a_W[9:, 18] = 1.009
    tbl_13a_W[9:, 19] = 0.

    tbl_13a_N_rhm = np.zeros((13,))
    tbl_13a_N_rhm[5:10] = [0.853, 0.918, 0.905, 1, 1]

    tbl_13a_W_rhm = np.zeros((13,))
    tbl_13a_W_rhm[5:10] = [1.007, 1.006, 1.006, 1.006, 1.006]

    sz_w_N = np.zeros((13, 24))
    sz_w_N[5:9, 7:20] = [0.334, 0.29, 0.3, 0.312, 0.324, 0.336, 0.342, 0.346, 0.343, 0.335, 0.325, 0.356, 0.402]
    sz_w_N[9, 7:20] = [0.171, 0.211, 0.249, 0.288, 0.321, 0.346, 0.362, 0.369, 0.366, 0.35, 0.325, 0.288, 0.233]

    sz_w_W = np.zeros((13, 24))
    sz_w_W[5:9, 7:20] = [0.119, 0.123, 0.127, 0.13, 0.133, 0.136, 0.152, 0.194, 0.241, 0.281, 0.308, 0.315, 0.291]
    sz_w_W[9, 7:20] = [0.092, 0.097, 0.101, 0.106, 0.11, 0.114, 0.132, 0.182, 0.235, 0.276, 0.290, 0.255, 0.169]

    for i, q in enumerate(q_ze):
        if orientation == Orientation.N:
            r_ht = tbl_13a_N[dates[i].month, dates[i].hour]
            s_zw = sz_w_N[dates[i].month, dates[i].hour]
            r_hm = tbl_13a_N_rhm[dates[i].month]
        elif orientation == Orientation.W:
            r_ht = tbl_13a_W[dates[i].month, dates[i].hour]
            s_zw = sz_w_W[dates[i].month, dates[i].hour]
            r_hm = tbl_13a_W_rhm[dates[i].month]
        else:
            r_ht = 0.
            s_zw = 0.
            r_hm = 0.
        q_sun[i] = Q_zg(
            ZTA=0.65,
            A_g=A,
            CF=0.11,  # Jellema Serie Bouwkunde Deel 4 Omhulling, vol. 4, 13 vols. ThiemeMeulenhoff bv, 2011.
            r_ht=r_ht,
            q_ze=q,
            s_zv=s_zv(A_w=A_w, s_zw=s_zw),
            r_hm=r_hm,
            q_ze_max=q_ze_max[i],
        )

    return q_sun
