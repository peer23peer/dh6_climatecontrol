from weather import *
from dimensions import *
from materials import *
from components import *
from simulation import *

from MTIpython.units.SI import u
from MTIpython.thermo.core import *
from MTIpython.thermo.network import *
from MTIpython.thermo.NEN5067 import *
from MTIpython.core import logging as mtilogging
mtilogging.setup_logging()
import logging
logging.getLogger().setLevel(logging.DEBUG)

import numpy as np
import matplotlib.pyplot as plt

u.default_format = '~P'
np.set_printoptions(precision=3)

aula = HeatStoreNode(name='aula', material=air_aula)
source1 = HeatSourceNode(name='source1')
source2 = HeatSourceNode(name='source2', q=1000. * u.W)
hf = HeatFlowNode(name='hf')

model = Model(nodes=[aula, source1, hf, source2])
#build_door(G=model.G, name='wall', A=97*u.m**2, sink='aula')
#build_outside_wall(G=model.G, name='wall', A=97*u.m**2, sink='aula')
model.add_edge(model.G, 'aula', 'source1', Edge())
model.add_edge(model.G, 'hf', 'source2', Edge())
model.add_edge(model.G, 'aula', 'hf', Edge())
model.init()

T_outside = [-10, -20, -10, -5, 0, 5, 10, 20, 10] * u.degC
T_inside = [0. for n in range(len(T_outside))] * u.degC

aula.q_c = 0. * u.W
model.aula.T = 0. * u.degC
#model.hf.T = 0. * u.degC
#model.aula.T = T_inside[0]
#model.nwall_door_0.T = T_outside[4]
#model.nwall_walloutside_0.T = T_outside[4]

G = model.solve()
